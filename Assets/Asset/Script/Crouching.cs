﻿using UnityEngine;
using System.Collections;

public class Crouching : MonoBehaviour {
    Animator animator;

	// Use this for initialization
	void Start () {
        animator = GetComponent<Animator>();

	}
	
	// Update is called once per frame
	void Update () {
	if (Input.GetKeyDown(KeyCode.C))
            {
            animator.SetBool("Crouching", true);
        }
        if (Input.GetKeyUp(KeyCode.C))
        {
            animator.SetBool("Crouching", false);
        }

    }
}
