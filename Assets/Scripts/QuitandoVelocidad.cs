﻿using UnityEngine;
using System.Collections;

public class QuitandoVelocidad : MonoBehaviour {

	Rigidbody rigidBody; 

	// Use this for initialization
	void Start () {

		rigidBody = GetComponent<Rigidbody> ();
	
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		rigidBody.velocity = Vector3.zero;
		rigidBody.angularVelocity = Vector3.zero;

	}




}
