﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PuntuacionIndividual : MonoBehaviour
{

    public static int puntos;
    public Text puntuacion;

    // Use this for initialization
    void Awake()
    {
        puntos = 0;
    }

    // Update is called once per frame
    void Update()
    {
        // Set the displayed text to be the word "Score" followed by the score value.
        puntuacion.text = "Puntuación: " + puntos;
    }
}