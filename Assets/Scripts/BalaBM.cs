﻿using UnityEngine;
using System.Collections;

public class BalaBM : MonoBehaviour {

    public int daño = 10;
    public string dañoA = "Player";

    void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.tag == dañoA)
        {
            VidaEnemigosBM vida = collision.collider.GetComponent<VidaEnemigosBM>();
            ContactPoint contacto = collision.contacts[0];
            if (vida != null)
            {
                vida.RecibeDaño(daño, contacto.point);
            }
        }
        Destroy(this.gameObject, 3f);

    }
}
