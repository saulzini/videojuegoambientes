﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class MenuManager : MonoBehaviour {
   
    //Selecciona el menu que va a estar abierto al principio en el Inspector
    public Menu menuActual;
    
	void Start () {
        MostrarMenu(menuActual);
	}
	
    public void MostrarMenu(Menu menu) {
        if (menuActual != null)
            menuActual.Abierto = false;

        menuActual = menu;
        menuActual.Abierto = true;
    }
	
    public void CargarNivel(string nombre)
    {
        Debug.Log(nombre);
        SceneManager.LoadScene(nombre);
    }
}
