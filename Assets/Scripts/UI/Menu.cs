﻿using System.Collections;
using UnityEngine;

public class Menu : MonoBehaviour {

    private Animator animador;
    private CanvasGroup canvas;

    public bool Abierto
    {
        get
        {
            return animador.GetBool("Abierto");
        }

        set
        {
            animador.SetBool("Abierto", value);
        }

    }

    public void Awake()
    {
        animador = GetComponent<Animator>();
        canvas = GetComponent<CanvasGroup>();

        var rect = GetComponent<RectTransform>();
        // Permite mover las ventanas a donde sea en design view pero en modo de juego se colocan en el centro
        rect.offsetMax = rect.offsetMin = new Vector2(0, 0);
    }

    public void Update()
    {
        //Si el controlador de la animacion no esta en el estado "Open" desahibilitamos el canvas group haciendolo no interactivo
        //Así aunque los diferentes menus están todos en el mismo lugar se desahibilitan los que no esten abiertos
        if (!animador.GetCurrentAnimatorStateInfo(0).IsName("Abierto"))
        {
            canvas.blocksRaycasts = canvas.interactable = false;
        }
        else
        {
            canvas.blocksRaycasts = canvas.interactable = true;
        }
    }
}
