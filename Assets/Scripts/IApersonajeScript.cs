﻿using UnityEngine;
using System.Collections;
using UnityEngine.VR;

public class IApersonajeScript : MonoBehaviour
{

    //Personaje
    public Rigidbody rigidbodyPersonaje; //Referencia al Rigidbody del personaje

    //Ejes
    float ejeX;
    float ejeY;

    //Salto
    [HideInInspector]
    public bool tocoTierra = false; // bandera para saber si esta en el aire o no (Salto)
    public float speeadSalto = 100f;
    float inclinacionMaxima = 60; //inclinación en salto largo

    //Fuerzas
    public float speed = 5f;
    public float force = 100f;

    //Disparo
    public float velocidadDisparo = 100;
    public float tiempoEspera; //tiempo de espera entre balas

    public GameObject bala;
    private GameObject bala1;
    public Transform balaPos;


    float x;
    float y;

    Vector3 personajeRelativo;
    public GameObject viewer;



    // Use this for initialization
    void Start()
    {
        rigidbodyPersonaje = GetComponent<Rigidbody>();
        //personaje = gameObject.transform;

    }

    // Update is called once per frame
    void Update()
    {
        //Movimiento
        ejeX = Input.GetAxis("Horizontal");
        ejeY = Input.GetAxis("Vertical");

        transform.Translate(new Vector3(ejeX, 0, ejeY) * speed * Time.deltaTime, viewer.transform);

        //Saltar
        if (Input.GetButtonDown("Salto") && tocoTierra)
            Salta();

        //Disparar
        if (Mathf.Round(Input.GetAxisRaw("Disparar")) > 0)
            Dispara();
    }

    void Dispara()
    {
        if (tiempoEspera <= 0)
        { //Tiempo para poder volver a disparar
          // Create the Bullet from the Bullet Prefab
            bala1 = (GameObject)Instantiate(bala, balaPos.position, balaPos.rotation);

            // Add velocity to the bullet
            bala1.GetComponent<Rigidbody>().AddForce(balaPos.transform.forward * force);
            tiempoEspera = 2;
        }

        tiempoEspera -= Time.deltaTime * velocidadDisparo; //Restar del tiempo en espera

    }

    void Salta()
    {
        rigidbodyPersonaje.AddForce(0, speeadSalto, 0);
    }


    /* Si el jugador esta colisionando con el piso entonces es posible saltar */
    void OnCollisionStay(Collision colision)
    {
        foreach (ContactPoint contacto in colision.contacts)
        {
            if (Vector3.Angle(contacto.normal, Vector3.up) < inclinacionMaxima)
                tocoTierra = true;
        }
    }

    /* Si no esta colisionando con el piso entonces es imposible realizar otro salto */
    void OnCollisionExit()
    {
        tocoTierra = false;
    }

}