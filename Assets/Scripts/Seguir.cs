﻿using UnityEngine;
using System.Collections;

public class Seguir : MonoBehaviour {

    public Transform objetoSeguir;
    public float distanciaZ = 10;
    
	// Update is called once per frame
	void Update () {
        Vector3 temp = transform.position;
        temp = objetoSeguir.position;
        temp.z = objetoSeguir.position.z + distanciaZ;
        transform.position = temp;
    }
}
