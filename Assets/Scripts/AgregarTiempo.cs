﻿using UnityEngine;
using System.Collections;

public class AgregarTiempo : MonoBehaviour {

    public float tiempoExtra = 10f; //Tiempo que se le va agregar por reloj

    //Posible cambio: En lugar de usar un collider tener que disparar o golpear los relojes
    void OnTriggerEnter(Collider other)
    {
        Mercenario.tiempoActual += tiempoExtra;
        Destroy(this.gameObject);
    }

}
