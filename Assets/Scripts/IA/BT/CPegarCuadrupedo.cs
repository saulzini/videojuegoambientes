﻿using UnityEngine;
using System.Collections;

public class CPegarCuadrupedo : MonoBehaviour {

	//creando instancia de enemigo
	EnemigoCuadrupedo enemigoScript;



	void Start(){



		if (enemigoScript == null) {
			enemigoScript = GetComponentInParent<EnemigoCuadrupedo>();	
		}


	}

    //Mientras el player esta dentro del collider entonces dispara
    void OnTriggerStay(Collider other)
    {
        //comparando si es el jugador
        if (other.gameObject.CompareTag("Player") == true)
        {
			enemigoScript.pega = true;
		
        }
    }

    //Si se salio del collider entonces es falso
    void OnTriggerExit(Collider other)
    {
        //comparando si es el jugador
        if (other.gameObject.CompareTag("Player") == true)
        {
			enemigoScript.pega = false;
        }
    }
}
