﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
public class EnemigoDeathMatch : MonoBehaviour {

	//variable para guardar el equipo
	[HideInInspector] public string EquipoAliado = "";
	[HideInInspector] public string EquipoEnemigo ="";

	//Nodeos a ocupar en el arbol
	public Selector nodoRaiz;
	public ActionNode patrullar;
	public Selector perseguir;
	public ActionNode seguir;
	public ActionNode disparar;
	public ActionNode pegar;
	public ActionNode esquivar;

	public float searchingTurnSpeed = 120f;
	public float searchingDuration = 4f;
	public float sightRange = 20f;

	public Transform vista;

	//Para apuntar a la cabeza del target y no a los pies
	public Vector3 offset = new Vector3(0,0.5f,0);

	//Variables de estados
	[HideInInspector] public Transform objetivoABuscar;
	public NavMeshAgent navMeshAgent;
	private int nextWayPoint;


	//Variables Bala
	public GameObject bala;
	public GameObject balaPegar;
	//Posicion para el cañon
	public GameObject posicionPistola;
	//velocidad bala
	public float fuerzaBala = 5000f;
	//Frecuencia de disparo
	public float frecuenciaDisparo = .15f;

	int rangoVista = 100;

	//temporizador
	float temporizador;


	//variables para disparar o pegar
	public  bool dispara = false;
	public  bool pega = false;

	/* We instantiate our nodes from the bottom up, and assign the children
     * in that order */
	void Start () {
		

		//obteniendo enemigo cercano
		GameObject enemigoEncontrado = EncontrarEnemigoCercano (EquipoEnemigo);
		objetivoABuscar=enemigoEncontrado.transform;

		//Se empieza con el nivel mas profundo	
		seguir = new ActionNode(PerseguirAccion);
		//disparar
		disparar = new ActionNode(DisparaBalaAccion);
		//pegar
		pegar = new ActionNode(PegarAccion);


		List<Node> raiz = new List<Node>();
		raiz.Add(seguir);
		raiz.Add(disparar);
		raiz.Add (pegar);

		/** Then we create our root node object and pass in the list* */
		nodoRaiz= new Selector(raiz);
		nodoRaiz.Evaluate();




	}


	void Update(){

		//obteniendo enemigo cercano
		GameObject enemigoEncontrado = EncontrarEnemigoCercano (EquipoEnemigo);
		objetivoABuscar=enemigoEncontrado.transform;


		//regresando la variable adecuada para saber si disparar o pegar
		if (pega == true && dispara == true) {
			dispara = false;

		}

	

		//metodos para que se ejecute el arbol
		nodoRaiz.Evaluate();

		//actualizando el temporizador
		temporizador += Time.deltaTime;



	}



	bool veoEnemigo(){


		//variable que guarda los objetos que esta viendo
		RaycastHit hit;

		//vector que se utiliza para ver enfrente
		Vector3 verEnfrente = vista.transform.TransformDirection(Vector3.forward);



		//Se crea una linea de frente al objeto para simular la vision y devuelve los objetos que ve en una variable llamada hit y revisa si es algun jugador
		if (Physics.Raycast (vista.transform.position, verEnfrente, out hit, rangoVista) ) 
		{
			Debug.DrawLine (vista.transform.position, hit.point, Color.cyan);

			//obteniendo el tag para saber si es enemigo
			if ( hit.collider.gameObject.tag == EquipoEnemigo ) {


					return true;

			
			} else {
				//se puede dar el caso que este viendo a algo que no sea el enemigo
				return false;
			}
		}

		return false;
	}



	NodeState.NodeStates  PerseguirAccion(){
		

		//variable que guarda los objetos que esta viendo
		RaycastHit hit;


		//Se crea una linea de frente al objeto para simular la vision y devuelve los objetos que ve en una variable llamada hit y revisa si es algun jugador
		if (  dispara == false && pega == false) {

		

			navMeshAgent.destination = objetivoABuscar.position;
			navMeshAgent.Resume ();


			//Si es cierto cambia a al estado de perseguir
			return NodeState.NodeStates.SUCCESS;

		} else {
			return NodeState.NodeStates.FAILURE;
		}
	}



	NodeState.NodeStates DisparaBalaAccion()
	{


		//Se crea una linea de frente al objeto para simular la vision y devuelve los objetos que ve en una variable llamada hit y revisa si es algun jugador
		if ( dispara == true && pega == false) {
			
			navMeshAgent.destination = objetivoABuscar.position;
			navMeshAgent.Resume ();


			//Validando la frecuencia de disparo
			if (temporizador >= frecuenciaDisparo){
				//reiniciando el temporizador
				temporizador = 0;

				// Crear instancia de la bala
				GameObject balaObjeto = Instantiate(bala, posicionPistola.transform.position, posicionPistola.transform.rotation) as GameObject;
				//Obteniendo el rigid body
				Rigidbody balaRigid = balaObjeto.GetComponent<Rigidbody>();
				//Añadiendo la velocidad
				balaRigid.velocity = fuerzaBala * posicionPistola.transform.forward;


				// Change the clip to the firing clip and play it.
				//m_ShootingAudio.clip = m_FireClip;
				//m_ShootingAudio.Play ();

				//regresando la variable dispara
				dispara = false;
			}


			return NodeState.NodeStates.SUCCESS;

		} else {
			return NodeState.NodeStates.FAILURE;
		}
	}

	NodeState.NodeStates PegarAccion()
	{
		
		//Se crea una linea de frente al objeto para simular la vision y devuelve los objetos que ve en una variable llamada hit y revisa si es algun jugador
		if ( dispara == false && pega == true)
		{
			
			//persiguiendo al personaje
			navMeshAgent.destination = objetivoABuscar.position;
			navMeshAgent.Resume();



			//Validando la frecuencia de disparo
			if (temporizador >= frecuenciaDisparo)
			{
				//reiniciando el temporizador
				temporizador = 0;

				// Crear instancia de la bala
				GameObject balaObjeto = Instantiate(balaPegar, posicionPistola.transform.position, posicionPistola.transform.rotation) as GameObject;
				//Obteniendo el rigid body
				Rigidbody balaRigid = balaObjeto.GetComponent<Rigidbody>();
				//Añadiendo la velocidad
				balaRigid.velocity = fuerzaBala * posicionPistola.transform.forward;


				// Change the clip to the firing clip and play it.
				//m_ShootingAudio.clip = m_FireClip;
				//m_ShootingAudio.Play ();

				//regresando la variable pega
				pega = false;
			}


			return NodeState.NodeStates.SUCCESS;

		}
		else
		{
			return NodeState.NodeStates.FAILURE;
		}
	}

	// Encontrar al enemigo más cercano

	GameObject EncontrarEnemigoCercano(string tagEnemigos) {
		//variable para guardar los enemigos
		GameObject[] enemigos;
		//obteniendo los enemigos que tengan cierto tag
		enemigos = GameObject.FindGameObjectsWithTag(EquipoEnemigo);
		//variable para guardar el nuevo enemigo a seguir
		GameObject enemigoCercano = null;
		//limpiando la  distancia con un numero muy grande 
		float distancia = Mathf.Infinity;
		//obtener la posicion de este personaje
		Vector3 posicion = transform.position;
		//ciclo para cada enemigo encontrado
		foreach (GameObject enemigo in enemigos) {
			//obteniendo la diferencia de distancia
			Vector3 diferenciaDistancia = enemigo.transform.position - posicion;
			//obteniendo la distancia con el enemigo (magnitud)
			float distanciaEnemigo = diferenciaDistancia.sqrMagnitude;

			//validando para que se obtenga el enemigo con menor distancia
			if (distanciaEnemigo < distancia) {
				enemigoCercano = enemigo;
				distancia = distanciaEnemigo;
			}
		}
		return enemigoCercano;
	}
}
