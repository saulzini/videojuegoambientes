﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
public class EnemigoCapturaBandera : MonoBehaviour {

	//variables estaticas para guardar los equipos y para comparar
	[HideInInspector] public string equipoRojo = "EquipoRojo";
	[HideInInspector] public string equipoAzul = "EquipoAzul";
	//variable para guardar el equipo
	[HideInInspector] public string EquipoAliado = "";
	[HideInInspector] public string EquipoEnemigo ="";
	[HideInInspector] public string BanderaAzul = "BanderaAzul";
	[HideInInspector] public string BanderaRojo = "BanderaRojo";



	//variable para guardar las banderas
	[HideInInspector] public Transform miBanderaPosicion;
	[HideInInspector] public Transform banderaEnemigoPosicion;
	[HideInInspector] public Transform miBasePosicion;
	[HideInInspector] public Transform baseEnemigaPosicion;

	//variable que guarda si tengo la banderaDelEquipo contrario
	[HideInInspector] public bool tengoBanderaEnemiga= false;

	public float rangoVista = 100f;
	public Transform vista;

	bool miBanderaCapturada;
	bool banderaEnemigoCapturada;
	bool miBanderaTirada;

	//Variables de estados
	public NavMeshAgent navMeshAgent;


	//Variables Bala
	public GameObject bala;
    public GameObject balaPegar;
	//Posicion para el cañon
	public GameObject posicionPistola;
	//velocidad bala
	public float fuerzaBala = 5000f;
	//Frecuencia de disparo
	public float frecuenciaDisparo = .15f;
	//temporizador
	float temporizador;


    //variables para disparar o pegar
	public  bool dispara = false;
    public  bool pega = false;

	//Nodos a ocupar en el arbol en orden descendente
	public Selector raiz;
	public ActionNode moverseABanderaEnemiga;
	public ActionNode regresarABase;
	public ActionNode apoyarEquipo;
	public Selector siVeoEnemigo;
	public ActionNode seguir;
	public ActionNode disparar;
	public ActionNode pegar;
	public Selector protegerBandera;
	public ActionNode seguirProtegerBandera;
	public ActionNode dispararProtegerBandera;
	public ActionNode pegarProtegerBandera;




	void Start () {

		actualizaDatos ();

		//Se empieza con el nivel mas profundo
		//Niveles para proteger si la bandera fue capturada
		seguirProtegerBandera = new ActionNode(SeguirProtegerBandera);
		//disparar
		dispararProtegerBandera  = new ActionNode(DispararProtegerBandera);
        //pegar
		pegarProtegerBandera  = new ActionNode(PegarProtegerBandera);


		//Añadiendo a la lista los nodos correspondientes del selector Proteger bandera
		List<Node> protegerBanderaNodos = new List<Node>();
		protegerBanderaNodos.Add(seguirProtegerBandera);
		protegerBanderaNodos.Add(dispararProtegerBandera);
		protegerBanderaNodos.Add(pegarProtegerBandera);

		//añadiendo al selector
		protegerBandera= new Selector(protegerBanderaNodos);
		protegerBandera.Evaluate();

		//Definiendo el siguiente nivel 
		seguir= new ActionNode(SeguirNoProtegerBandera);
		//disparar
		disparar= new ActionNode(DisparaNoProtegerBandera);
		//pegar
		pegar= new ActionNode(PegarNoProtegerBandera);

		List<Node> siVeoEnemigoNodos = new List<Node>();
		siVeoEnemigoNodos.Add (seguir);
		siVeoEnemigoNodos.Add (disparar);
		siVeoEnemigoNodos.Add (pegar);

		siVeoEnemigo = new Selector (siVeoEnemigoNodos);
		siVeoEnemigo.Evaluate ();

		//Definiendo el siguiente nivel
		moverseABanderaEnemiga = new ActionNode(MoverseABanderaEnemiga);
		regresarABase = new ActionNode(RegresarABaseAliada);
		apoyarEquipo = new ActionNode (ApoyarEquipo);


		List<Node> raizNodos = new List<Node>();
		raizNodos.Add(moverseABanderaEnemiga);
		raizNodos.Add(regresarABase);
		raizNodos.Add(siVeoEnemigo);
		raizNodos.Add(protegerBandera);
		raizNodos.Add (apoyarEquipo);
		// Then we create our root node object and pass in the list//
		raiz  = new Selector(raizNodos);
		raiz.Evaluate();

		

	
	}



	//metodo para actualizar las posiciones de la bandera
	void actualizaDatos(){

		//validando de acuerdo al color equipo
		if (EquipoAliado == equipoAzul) {
			//guardando variables

			miBanderaPosicion = GameObject.FindGameObjectWithTag (BanderaAzul).transform;
			banderaEnemigoPosicion = GameObject.FindGameObjectWithTag (BanderaRojo).transform;

			miBanderaCapturada = CapturaBandera.banderaAzulCapturada;
			banderaEnemigoCapturada = CapturaBandera.banderaRojoCapturada;

			miBanderaTirada = CapturaBandera.banderaAzulTirada;

		} else {
			//guardando variables

			miBanderaPosicion = GameObject.FindGameObjectWithTag (BanderaRojo).transform;
			banderaEnemigoPosicion = GameObject.FindGameObjectWithTag (BanderaAzul).transform;
			miBanderaCapturada = CapturaBandera.banderaRojoCapturada;
			banderaEnemigoCapturada = CapturaBandera.banderaAzulCapturada;
			miBanderaTirada = CapturaBandera.banderaRojoTirada;
		}




	}

	void Update(){
		
		actualizaDatos();

		//regresando la variable adecuada para saber si disparar o pegar
		if (pega == true && dispara == true) {
			dispara = false;
		}
			  

		//metodos para que se ejecute el arbol
		siVeoEnemigo.Evaluate ();
		protegerBandera.Evaluate();
		raiz.Evaluate();

		//actualizando el temporizador
		temporizador += Time.deltaTime;


	}


	NodeState.NodeStates  MoverseABanderaEnemiga(){

		//valida si no hay bandera capturada para que siga buscando 
		if ( banderaEnemigoCapturada ==false && miBanderaCapturada == false && veoEnemigo()==false) {

			//posicion a la que tiene que ir
			navMeshAgent.destination = banderaEnemigoPosicion.position;
			navMeshAgent.Resume ();

			return NodeState.NodeStates.SUCCESS;

		} else {
			return NodeState.NodeStates.FAILURE;
		}
	}

	NodeState.NodeStates  RegresarABaseAliada(){

		//valida si no hay bandera capturada para que siga buscando 
		if (  tengoBanderaEnemiga == true   && veoEnemigo() == false  ) {

			//posicion a la que tiene que ir
			navMeshAgent.destination = miBasePosicion.position;
			navMeshAgent.Resume ();


			//Si es cierto cambia a al estado de perseguir
			return NodeState.NodeStates.SUCCESS;

		} else {
			return NodeState.NodeStates.FAILURE;
		}
	}

	NodeState.NodeStates ApoyarEquipo(){


		if (banderaEnemigoCapturada == true && miBanderaCapturada == false && veoEnemigo()==false) {
			//posicion a la que tiene que ir
			navMeshAgent.destination = banderaEnemigoPosicion.position;
			navMeshAgent.Resume ();


			//Si es cierto cambia a al estado de perseguir
			return NodeState.NodeStates.SUCCESS;
			
		}
		else {
			return NodeState.NodeStates.FAILURE;
		}


	}
		
	bool veoEnemigo(){


		//variable que guarda los objetos que esta viendo
		RaycastHit hit;

		//vector que se utiliza para ver enfrente
		Vector3 verEnfrente = vista.transform.TransformDirection(Vector3.forward);

		//Se crea una linea de frente al objeto para simular la vision y devuelve los objetos que ve en una variable llamada hit y revisa si es algun jugador
		if (Physics.Raycast (vista.transform.position, verEnfrente, out hit, rangoVista) ) 
		{
			Debug.DrawLine (vista.transform.position, hit.point, Color.cyan);
				

				if (hit.collider.gameObject.tag == EquipoEnemigo){

					return true;
				
				} 
				
				else {
				
					return false;
				}

		} 
		else {
			//se puede dar el caso que este viendo a algo que no sea el enemigo
				return false;
		}


			
	}
		
	NodeState.NodeStates  SeguirProtegerBandera(){

	
		//No es necesario que vea al enemigo que tiene la bandera solo va directo a el que tiene la bandera 

		//Se crea una linea de frente al objeto para simular la vision y devuelve los objetos que ve en una variable llamada hit y revisa si es algun jugador
		if ( dispara == false && pega == false  && ( miBanderaCapturada == true) ) {



			navMeshAgent.destination = miBanderaPosicion.position;



			navMeshAgent.Resume ();


			//Si es cierto cambia a al estado de perseguir
			return NodeState.NodeStates.SUCCESS;

		} else {
			return NodeState.NodeStates.FAILURE;
		}
	}
		
	NodeState.NodeStates DispararProtegerBandera()
	{


		//Se crea una linea de frente al objeto para simular la vision y devuelve los objetos que ve en una variable llamada hit y revisa si es algun jugador
		if (  dispara == true && pega == false && ( miBanderaCapturada == true ) && veoEnemigo()==true) {


			//Validando la frecuencia de disparo
			if (temporizador >= frecuenciaDisparo){
				
				//reiniciando el temporizador
				temporizador = 0;

				// Crear instancia de la bala
				GameObject balaObjeto = Instantiate(bala, posicionPistola.transform.position, posicionPistola.transform.rotation) as GameObject;
				//Obteniendo el rigid body
				Rigidbody balaRigid = balaObjeto.GetComponent<Rigidbody>();
				//Añadiendo la velocidad
				balaRigid.velocity = fuerzaBala * posicionPistola.transform.forward;


				//reproducir sonido
				//m_ShootingAudio.clip = m_FireClip;
				//m_ShootingAudio.Play ();

				//regresando la variable dispara
				dispara = false;
			}


			return NodeState.NodeStates.SUCCESS;

		} else {
			return NodeState.NodeStates.FAILURE;
		}
	}
		
	NodeState.NodeStates PegarProtegerBandera()
	{

		//Se crea una linea de frente al objeto para simular la vision y devuelve los objetos que ve en una variable llamada hit y revisa si es algun jugador
		if ( veoEnemigo()==true  && dispara == false && pega == true && ( miBanderaCapturada == true ))
		{
			
			//Validando la frecuencia de disparo
			if (temporizador >= frecuenciaDisparo)
			{
				//reiniciando el temporizador
				temporizador = 0;

				// Crear instancia de la bala
				GameObject balaObjeto = Instantiate(balaPegar, posicionPistola.transform.position, posicionPistola.transform.rotation) as GameObject;
				//Obteniendo el rigid body
				Rigidbody balaRigid = balaObjeto.GetComponent<Rigidbody>();
				//Añadiendo la velocidad
				balaRigid.velocity = fuerzaBala * posicionPistola.transform.forward;


				// Reproducir sonido
				//m_ShootingAudio.clip = m_FireClip;
				//m_ShootingAudio.Play ();

				//regresando la variable pega
				pega = false;
			}


			return NodeState.NodeStates.SUCCESS;

		}
		else
		{
			return NodeState.NodeStates.FAILURE;
		}
	}
		
	NodeState.NodeStates SeguirNoProtegerBandera(){
		
		//variable que guarda los objetos que esta viendo
		//Se crea una linea de frente al objeto para simular la vision y devuelve los objetos que ve en una variable llamada hit y revisa si es algun jugador
		if (veoEnemigo() ==true && dispara == false && pega == false && miBanderaCapturada == false ) {


			//Si es cierto cambia a al estado de perseguir
			return NodeState.NodeStates.SUCCESS;

		} else {
			return NodeState.NodeStates.FAILURE;
		}
	}

	NodeState.NodeStates DisparaNoProtegerBandera()
	{
        
       
		//Se crea una linea de frente al objeto para simular la vision y devuelve los objetos que ve en una variable llamada hit y revisa si es algun jugador
		if (veoEnemigo()==true && dispara == true && pega == false && miBanderaCapturada == false ) {

			//Validando la frecuencia de disparo
			if (temporizador >= frecuenciaDisparo){
				
				//reiniciando el temporizador
				temporizador = 0;

				// Crear instancia de la bala
				GameObject balaObjeto = Instantiate(bala, posicionPistola.transform.position, posicionPistola.transform.rotation) as GameObject;
				//Obteniendo el rigid body
				Rigidbody balaRigid = balaObjeto.GetComponent<Rigidbody>();
				//Añadiendo la velocidad
				balaRigid.velocity = fuerzaBala * posicionPistola.transform.forward;
              
				// reproducir sonido
				//m_ShootingAudio.clip = m_FireClip;
				//m_ShootingAudio.Play ();

				//regresando la variable dispara
				dispara = false;
			}


			return NodeState.NodeStates.SUCCESS;

		} else {
			return NodeState.NodeStates.FAILURE;
		}
	}

	NodeState.NodeStates PegarNoProtegerBandera()
    {
        //Se crea una linea de frente al objeto para simular la vision y devuelve los objetos que ve en una variable llamada hit y revisa si es algun jugador
		if (veoEnemigo()==true && dispara == false && pega == true && miBanderaCapturada == false)
        {
           

            //Validando la frecuencia de disparo
            if (temporizador >= frecuenciaDisparo)
            {
                //reiniciando el temporizador
                temporizador = 0;

                // Crear instancia de la bala
                GameObject balaObjeto = Instantiate(balaPegar, posicionPistola.transform.position, posicionPistola.transform.rotation) as GameObject;
                //Obteniendo el rigid body
                Rigidbody balaRigid = balaObjeto.GetComponent<Rigidbody>();
                //Añadiendo la velocidad
                balaRigid.velocity = fuerzaBala * posicionPistola.transform.forward;


                // reproducir sonido
                //m_ShootingAudio.clip = m_FireClip;
                //m_ShootingAudio.Play ();

                //regresando la variable pega
                pega = false;
            }


            return NodeState.NodeStates.SUCCESS;

        }
        else
        {
            return NodeState.NodeStates.FAILURE;
        }
    }


	//Si se destruye el gameobject y si tiene la bandera entonces crear la bandera en ese punto
	void OnDestroy(){
		
		if (tengoBanderaEnemiga == true ) {
			
			//validando de que equipo soy
			//para quitar al hijo
			if (EquipoEnemigo == equipoAzul) {
				
				GameObject bandera = gameObject.transform.FindChild(BanderaAzul).gameObject;
				Bandera banderaScript = bandera.GetComponent<Bandera> ();
				banderaScript.enabled = true;
				bandera.transform.SetParent (null);
			
			} else {
				GameObject bandera = gameObject.transform.FindChild(BanderaRojo).gameObject;
				bandera.transform.SetParent (null);
				Bandera banderaScript = bandera.GetComponent<Bandera> ();
				banderaScript.enabled = true;
		
			}

			tengoBanderaEnemiga = false;

		}
	
	}

}
