﻿using UnityEngine;
using System.Collections;

public class CDisparaCapturaBandera : MonoBehaviour
{

	//Instancia del script 
	EnemigoCapturaBandera enemigoScript;

	void Awake (){
	
		if (enemigoScript == null) {
			enemigoScript = GetComponentInParent<EnemigoCapturaBandera>();
		}


	}

	void Start(){



	}

    //Mientras el player esta dentro del collider entonces dispara
    void OnTriggerStay(Collider other)
    {
		
        //comparando si es el jugador
		if ( other.gameObject.tag == enemigoScript.EquipoEnemigo   )
        {
			
				enemigoScript.dispara = true;

        }
    }

    //Si se salio del collider entonces es falso
    void OnTriggerExit(Collider other)
    {
        //comparando si es el jugador
		if (other.gameObject.tag == enemigoScript.EquipoEnemigo )
        {

			enemigoScript.dispara = false;



        }
    }

}