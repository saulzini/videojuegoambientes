﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EnemigoDestruyeBase : MonoBehaviour {

    //variable para guardar el equipo
    [HideInInspector] public string EquipoAliado = "";
    [HideInInspector] public string EquipoEnemigo = "";

    //Nodos a ocupar en el arbol
    public Selector nodoRaiz;
    public ActionNode defender;
    public ActionNode irBaseEnemiga;
    public Selector colision;
    public ActionNode disparar;
    public ActionNode pegar;
    public Selector seguirRecolectar;
    public ActionNode seguir;
    public ActionNode recolectarBomba;

    public float searchingTurnSpeed = 120f;
    public float searchingDuration = 4f;
    public float sightRange = 20f;

    public Transform vista;

    //Para apuntar a la cabeza del target y no a los pies
    public Vector3 offset = new Vector3(0, 0.5f, 0);

    //Variables de estados
    [HideInInspector] public Transform enemigoABuscar;
    [HideInInspector] public Transform bombaABuscar;
    //Para saber si seguir al enemigo o recolectar bomba
    [HideInInspector] public string tipoObjectoABuscar;
    [HideInInspector] public bool tengoBomba;
    [HideInInspector] public bool bombaEnMiBase;
    [HideInInspector] public bool bombaEnBaseEnemiga;
    [HideInInspector] public bool estoyEnBaseEnemiga;
    [HideInInspector] public bool estoyEnMiBase;
    [HideInInspector] public static string bombaEnBase;
    [HideInInspector] public GameObject bomba;
    [HideInInspector] public GameObject baseEnemiga;
    [HideInInspector] public GameObject miBase;
    [HideInInspector] public string baseCercana;
    public NavMeshAgent navMeshAgent;
    private int nextWayPoint;


    //Variables Bala
    public GameObject bala;
    public GameObject balaPegar;
    //Posicion para el cañon
    public GameObject posicionPistola;
    //velocidad bala
    public float fuerzaBala = 5000f;
    //Frecuencia de disparo
    public float frecuenciaDisparo = .15f;

    int rangoVista = 100;

    //temporizador
    float temporizador;


    //variables para disparar o pegar
    public bool dispara = false;
    public bool pega = false;

    /* We instantiate our nodes from the bottom up, and assign the children
     * in that order */
    void Start()
    {
        //Se empieza con el nivel mas profundo	
        disparar = new ActionNode(DisparaBalaAccion);
        pegar = new ActionNode(PegarAccion);
        seguir = new ActionNode(PerseguirAccion);
        recolectarBomba = new ActionNode(RecolectarAccion);
        defender = new ActionNode(DefenderAccion);
        irBaseEnemiga = new ActionNode(IrBaseEnemigaAccion);

        //seguirRecolectarNodos
        //Añadir nodos al primer selector
        List<Node> colisionNodos = new List<Node>();
        colisionNodos.Add(disparar);
        colisionNodos.Add(pegar);

        //Añadir nodos al segundo selector
        /*List<Node> seguirRecolectarNodos = new List<Node>();
        seguirRecolectarNodos.Add(seguir);
        seguirRecolectarNodos.Add(recolectarBomba);*/

        // Crear selectores
        colision = new Selector(colisionNodos);
        colision.Evaluate();

        /*seguirRecolectar = new Selector(seguirRecolectarNodos);
        seguirRecolectar.Evaluate();*/

        List<Node> raiz = new List<Node>();
        raiz.Add(colision);
        //raiz.Add(seguirRecolectar);
        raiz.Add(recolectarBomba);
        raiz.Add(defender);
        raiz.Add(irBaseEnemiga);
        raiz.Add(seguir);
        /*Crear el nodo raíz*/
        nodoRaiz = new Selector(raiz);
        //nodoRaiz.Evaluate();
    }


    void Update()
    {
        //regresando la variable adecuada para saber si disparar o pegar
        if (pega == true && dispara == true)
        {
            dispara = false;

        }

        //obteniendo enemigo cercano
        GameObject enemigoEncontrado = EncontrarEnemigoCercano(EquipoEnemigo);
        GameObject bombaEncontrada = EncontrarBombaCercana();
        if (bombaEncontrada != null && enemigoEncontrado != null)
            tipoObjectoABuscar = EncontrarMasCercano(enemigoEncontrado, bombaEncontrada);
        else if (bombaEncontrada == null)
            tipoObjectoABuscar = enemigoEncontrado.tag;
        
        enemigoABuscar = enemigoEncontrado.transform;
        if(bombaEncontrada != null)
            bombaABuscar = bombaEncontrada.transform;
        baseCercana = EncontrarMasCercano(miBase, baseEnemiga);

        //tengo bomba?
        tengoBomba = BuscarSiTengoBomba();

        //metodos para que se ejecute el arbol
        nodoRaiz.Evaluate();

        //actualizando el temporizador
        temporizador += Time.deltaTime;

    }

    bool veoEnemigo()
    {
        //variable que guarda los objetos que esta viendo
        RaycastHit hit;

        //vector que se utiliza para ver enfrente
        Vector3 verEnfrente = vista.transform.TransformDirection(Vector3.forward);

        //Se crea una linea de frente al objeto para simular la vision y devuelve los objetos que ve en una variable llamada hit y revisa si es algun jugador
        if (Physics.Raycast(vista.transform.position, verEnfrente, out hit, rangoVista))
        {
            Debug.DrawLine(vista.transform.position, hit.point, Color.cyan);

            //obteniendo el tag para saber si es enemigo
            if (hit.collider.gameObject.tag == EquipoEnemigo)
            {
                return true;
            }
            else
            {
                //se puede dar el caso que este viendo a algo que no sea el enemigo
                return false;
            }
        }
        return false;
    }


    NodeState.NodeStates PerseguirAccion()
    {
        //if (dispara == false && pega == false)
        //if(veoEnemigo() && tipoObjectoABuscar == EquipoEnemigo)
        if(tengoBomba && bombaABuscar == null)
        {
            print("perseguir success");
            navMeshAgent.destination = enemigoABuscar.position;
            navMeshAgent.Resume();
            //Si es cierto cambia a al estado de perseguir
            return NodeState.NodeStates.SUCCESS;
        }
        else
        {
            return NodeState.NodeStates.FAILURE;
        }
    }

    NodeState.NodeStates RecolectarAccion()
    {
        //if (tipoObjectoABuscar == "Bomba" && !tengoBomba)
        if(!tengoBomba && bombaABuscar != null)
        {
            print("recolectar bomba success");
            navMeshAgent.destination = bombaABuscar.position;
            navMeshAgent.Resume();

            return NodeState.NodeStates.SUCCESS;
        }
        else
        {
            return NodeState.NodeStates.FAILURE;
        }
    }



    NodeState.NodeStates DisparaBalaAccion()
    {
        //Se crea una linea de frente al objeto para simular la vision y devuelve los objetos que ve en una variable llamada hit y revisa si es algun jugador
        if (dispara && !pega && veoEnemigo())
        {
            print("dispara success");
            navMeshAgent.destination = enemigoABuscar.position;
            navMeshAgent.Resume();

            //Validando la frecuencia de disparo
            if (temporizador >= frecuenciaDisparo)
            {
                //reiniciando el temporizador
                temporizador = 0;

                // Crear instancia de la bala
                GameObject balaObjeto = Instantiate(bala, posicionPistola.transform.position, posicionPistola.transform.rotation) as GameObject;
                //Obteniendo el rigid body
                Rigidbody balaRigid = balaObjeto.GetComponent<Rigidbody>();
                //Añadiendo la velocidad
                balaRigid.velocity = fuerzaBala * posicionPistola.transform.forward;


                // Change the clip to the firing clip and play it.
                //m_ShootingAudio.clip = m_FireClip;
                //m_ShootingAudio.Play ();

                //regresando la variable dispara
                dispara = false;
            }
            return NodeState.NodeStates.SUCCESS;
        }
        else
        {
            return NodeState.NodeStates.FAILURE;
        }
    }

    NodeState.NodeStates PegarAccion()
    {
        //Se crea una linea de frente al objeto para simular la vision y devuelve los objetos que ve en una variable llamada hit y revisa si es algun jugador
        if (!dispara && pega && veoEnemigo())
        {
            print("Pegar success");
            //persiguiendo al personaje
            navMeshAgent.destination = enemigoABuscar.position;
            navMeshAgent.Resume();

            //Validando la frecuencia de disparo
            if (temporizador >= frecuenciaDisparo)
            {
                //reiniciando el temporizador
                temporizador = 0;
                // Crear instancia de la bala
                GameObject balaObjeto = Instantiate(balaPegar, posicionPistola.transform.position, posicionPistola.transform.rotation) as GameObject;
                //Obteniendo el rigid body
                Rigidbody balaRigid = balaObjeto.GetComponent<Rigidbody>();
                //Añadiendo la velocidad
                balaRigid.velocity = fuerzaBala * posicionPistola.transform.forward;

                //regresando la variable pega
                pega = false;
            }
            return NodeState.NodeStates.SUCCESS;
        }
        else
        {
            return NodeState.NodeStates.FAILURE;
        }
    }

    NodeState.NodeStates DefenderAccion()
    {
        //if(bombaEnMiBase && !tengoBomba && !estoyEnMiBase)
        if(!tengoBomba && bombaEnMiBase)
        {
            print("defender base success");
            //corutina para regresar a la base y soltar la bomba
            navMeshAgent.destination = miBase.transform.position;
            navMeshAgent.Resume();
            return NodeState.NodeStates.SUCCESS;
        }
        /*else if (bombaEnMiBase && baseCercana == miBase.tag && !estoyEnMiBase)
        {
            print("defender base success");
            //soltar bomba
            soltarBomba();
            //regresar a la base
            navMeshAgent.destination = miBase.transform.position;
            navMeshAgent.Resume();
            return NodeState.NodeStates.SUCCESS;
        }*/
        else
        {
            return NodeState.NodeStates.FAILURE;
        }
    }

    NodeState.NodeStates IrBaseEnemigaAccion()
    {
        //if((tengoBomba && !bombaEnBaseEnemiga && !estoyEnBaseEnemiga) || (tengoBomba && bombaEnMiBase && baseCercana == baseEnemiga.tag && !bombaEnBaseEnemiga && !estoyEnBaseEnemiga))
        if(tengoBomba)
        {
            print("ir a base enemiga success");
            print("Estoy en base enemiga?: " + estoyEnBaseEnemiga);
            if (estoyEnBaseEnemiga)
            {
                
                soltarBomba();

            }
            else
            {
                navMeshAgent.destination = baseEnemiga.transform.position;
                navMeshAgent.Resume();
            }
            return NodeState.NodeStates.SUCCESS;
        }
        else
        {
            return NodeState.NodeStates.FAILURE;
        }

    }



    // Encontrar al enemigo más cercano
    GameObject EncontrarEnemigoCercano(string tagEnemigos)
    {
        //variable para guardar los enemigos
        GameObject[] enemigos;
        //obteniendo los enemigos que tengan cierto tag
        enemigos = GameObject.FindGameObjectsWithTag(EquipoEnemigo);
        //variable para guardar el nuevo enemigo a seguir
        GameObject enemigoCercano = null;
        //limpiando la  distancia con un numero muy grande 
        float distancia = Mathf.Infinity;
        //obtener la posicion de este personaje
        Vector3 posicion = transform.position;
        //ciclo para cada enemigo encontrado
        foreach (GameObject enemigo in enemigos)
        {
            //obteniendo la diferencia de distancia
            Vector3 diferenciaDistancia = enemigo.transform.position - posicion;
            //obteniendo la distancia con el enemigo (magnitud)
            float distanciaEnemigo = diferenciaDistancia.sqrMagnitude;

            //validando para que se obtenga el enemigo con menor distancia
            if (distanciaEnemigo < distancia)
            {
                enemigoCercano = enemigo;
                distancia = distanciaEnemigo;
            }
        }
        return enemigoCercano;
    }

    GameObject EncontrarBombaCercana()
    {
        //variable para guardar las bombas
        GameObject[] bombas;
        //obteniendo las bombas por tag
        bombas = GameObject.FindGameObjectsWithTag("Bomba");
        //variable para guardar la nueva bomba más cercana
        GameObject bombaCercana = null;
        //limpiando la  distancia con un numero muy grande 
        float distancia = Mathf.Infinity;
        //obtener la posicion de este personaje
        Vector3 posicion = transform.position;
        //ciclo para cada bomba encontrada
        foreach (GameObject bomba in bombas)
        {
            if (bomba.transform.parent == null && !bombaEnBaseEnemiga)
            {
                //obteniendo la diferencia de distancia
                Vector3 diferenciaDistancia = bomba.transform.position - posicion;
                //obteniendo la distancia con la bomba (magnitud)
                float distanciaBomba = diferenciaDistancia.sqrMagnitude;

                //validando para que se obtenga el enemigo con menor distancia
                if (distanciaBomba < distancia)
                {
                    bombaCercana = bomba;
                    distancia = distanciaBomba;
                }
            }
            
        }
        return bombaCercana;
    }

    string EncontrarMasCercano(GameObject objeto1, GameObject objeto2)
    {
        //obtener la posicion de este personaje
        Vector3 posicion = transform.position;
        //obteniendo las diferencia de distancia
        Vector3 distObj1 = objeto1.transform.position - posicion;
        Vector3 distObj2 = objeto2.transform.position - posicion;
        //obteniendo las distancias (magnitud)
        float distancia1 = distObj1.sqrMagnitude;
        float distancia2 = distObj2.sqrMagnitude;

        if (distancia1 < distancia2)
            return objeto1.tag;
        else if (distancia2 < distancia1)
            return objeto2.tag;
        else
            return objeto2.tag;
    }

    bool BuscarSiTengoBomba()
    {
        bomba = null;
        //Encontrar si tiene una bomba como hijo
        foreach (Transform tr in transform)
        {
            if (tr.tag == "Bomba")
                bomba = tr.gameObject;
        }

        if (bomba == null)
            return false;
        else
            return true;
    }

    void soltarBomba()
    {
        if (tengoBomba)
        {
            print("Soltar bomba");
            //bomba.transform.position += Vector3.up * 20 * Time.deltaTime;
            bomba.transform.parent = null;
        }
    }
    
}
