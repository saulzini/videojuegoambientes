﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Sequence : Node {

    /** Chiildren nodes that belong to this sequence */
    private List<Node> m_nodes = new List<Node>();

    /** Must provide an initial set of children nodes to work */
    public Sequence(List<Node> nodes)
    {
        m_nodes = nodes;
    }

    /* If any child node returns a failure, the entire node fails. Whence all 
     * nodes return a success, the node reports a success. */
    public override NodeState.NodeStates Evaluate()
    {
        bool anyChildRunning = false;

        foreach (Node node in m_nodes)
        {
            switch (node.Evaluate())
            {
                case NodeState.NodeStates.FAILURE:
                    m_nodeState = NodeState.NodeStates.FAILURE;
                    return m_nodeState;
                case NodeState.NodeStates.SUCCESS:
                    continue;
                case NodeState.NodeStates.RUNNING:
                    anyChildRunning = true;
                    continue;
                default:
                    m_nodeState = NodeState.NodeStates.SUCCESS;
                    return m_nodeState;
            }
        }
        m_nodeState = anyChildRunning ? NodeState.NodeStates.RUNNING : NodeState.NodeStates.SUCCESS;
        return m_nodeState;
    }
}
