﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
public class EnemigoCuadrupedo : MonoBehaviour {

	//variable para guardar el equipo
	[HideInInspector] public string EquipoAliado = "";
	[HideInInspector] public string EquipoEnemigo ="Player";

	//Nodeos a ocupar en el arbol
	public Selector nodoRaiz;
	public ActionNode patrullar;
	public Selector perseguir;
	public ActionNode seguir;
	public ActionNode pegar;
	public ActionNode esquivar;

	public float searchingTurnSpeed = 120f;
	public float searchingDuration = 4f;
	public float sightRange = 20f;
	public Transform[] wayPoints;
	public Transform vista;

    //Para apuntar a la cabeza del target y no a los pies
    public Vector3 offset = new Vector3(0,0.5f,0);

	//Variables de estados
	[HideInInspector] public Transform objetivoABuscar;
	public NavMeshAgent navMeshAgent;
	private int nextWayPoint;


	//Variables Bala
	public GameObject bala;
    public GameObject balaPegar;
	//Posicion para el cañon
	public GameObject posicionPistola;
	//velocidad bala
	public float fuerzaBala = 5000f;
	//Frecuencia de disparo
	public float frecuenciaDisparo = .15f;


	//temporizador
	float temporizador;


    //variables para disparar o pegar
    public  bool pega = false;




	/* We instantiate our nodes from the bottom up, and assign the children
     * in that order */
	void Start () {
		
		//obteniendo enemigo cercano
		GameObject enemigoEncontrado = EncontrarEnemigoCercano (EquipoEnemigo);
		objetivoABuscar=enemigoEncontrado.transform;

		//Se empieza con el nivel mas profundo	
		seguir = new ActionNode(PerseguirAccion);

        //pegar
        pegar = new ActionNode(PegarAccion);

		//accion
		patrullar = new ActionNode(PatrullarAccion);

		/** Lastly, we have our root node. First, we prepare our list of children
         * nodes to pass in */
		List<Node> perseguirNodos = new List<Node>();
		perseguirNodos.Add(seguir);
        perseguirNodos.Add(pegar);

		/** Then we create our root node object and pass in the list* */
		perseguir= new Selector(perseguirNodos);
		perseguir.Evaluate();

		List<Node> raiz = new List<Node>();
		raiz.Add(patrullar);
		raiz.Add(perseguir);

		/** Then we create our root node object and pass in the list* */
		nodoRaiz= new Selector(raiz);
		nodoRaiz.Evaluate();




	}


	void Update(){


		//obteniendo enemigo cercano
		GameObject enemigoEncontrado = EncontrarEnemigoCercano (EquipoEnemigo);
		objetivoABuscar=enemigoEncontrado.transform;

		//metodos para que se ejecute el arbol
		perseguir.Evaluate();
		nodoRaiz.Evaluate();

		//actualizando el temporizador
		temporizador += Time.deltaTime;



	}


	NodeState.NodeStates  PerseguirAccion(){
		
		//variable que guarda los objetos que esta viendo
		RaycastHit hit;

		//Generando un vector para que pueda ver al jugador directamente a la cabeza
		Vector3 enemigoObjetivo = ((objetivoABuscar.position + offset) - vista.transform.position) ;

		//Se crea una linea de frente al objeto para simular la vision y devuelve los objetos que ve en una variable llamada hit y revisa si es algun jugador
		if (Physics.Raycast (vista.transform.position, enemigoObjetivo, out hit, sightRange) && hit.collider.CompareTag ("Player")  && pega == false) {

			Debug.DrawLine (transform.position, hit.point, Color.cyan);

			navMeshAgent.destination = objetivoABuscar.position;
			navMeshAgent.Resume ();

			//Le pasas la posicion a la que tiene que ir
			objetivoABuscar = hit.transform;
			//Si es cierto cambia a al estado de perseguir
			return NodeState.NodeStates.SUCCESS;

		} else {
			return NodeState.NodeStates.FAILURE;
		}
	}

	NodeState.NodeStates PatrullarAccion (){

	//	print("patrullar");
		//variable que guarda los objetos que esta viendo
		RaycastHit hit;


		//Generando un vector para que pueda ver al jugador directamente a la cabeza
		Vector3 enemyToTarget = ((objetivoABuscar.position + offset) - vista.transform.position) ;

		//Se crea una linea de frente al objeto para simular la vision y devuelve los objetos que ve en una variable llamada hit y revisa si es algun jugador
		if (Physics.Raycast (vista.transform.position, enemyToTarget, out hit, sightRange) && hit.collider.CompareTag ("Player") ) {

			//Le pasas la posicion a la que tiene que ir
			objetivoABuscar = hit.transform;
			//Si es cierto cambia a al estado de perseguir
			return NodeState.NodeStates.SUCCESS;

		} else {

			//Pone como destino el siguiente waypoint
			navMeshAgent.destination = wayPoints [nextWayPoint].position;
			//Para que haga la accion
			navMeshAgent.Resume ();


			// si el enemigo ya no tiene mas que caminar entonces avanza al siguiente punto
			if (navMeshAgent.remainingDistance <= navMeshAgent.stoppingDistance && !navMeshAgent.pathPending) {
				//se ocupa modulo para que vaya al siguiente waypoint y se regrese al principio si llegas al ultimo
				nextWayPoint = (nextWayPoint + 1) % wayPoints.Length;

			}
			return NodeState.NodeStates.FAILURE;
		}
	}




    NodeState.NodeStates PegarAccion()
    {

        //variable que guarda los objetos que esta viendo
        RaycastHit hit;

        //Generando un vector para que pueda ver al jugador directamente a la cabeza
		Vector3 enemigoObjetivo = ((objetivoABuscar.position + offset) - vista.transform.position);

        //Se crea una linea de frente al objeto para simular la vision y devuelve los objetos que ve en una variable llamada hit y revisa si es algun jugador
		if (Physics.Raycast(vista.transform.position, enemigoObjetivo, out hit, sightRange) && hit.collider.CompareTag("Player")  && pega == true)
        {
            //print("pegar");
            //dibujar linea de vista
            Debug.DrawLine(transform.position, hit.point, Color.cyan);
            //persiguiendo al personaje
			navMeshAgent.destination = objetivoABuscar.position;
            navMeshAgent.Resume();

            //Le pasas la posicion a la que tiene que ir
			objetivoABuscar = hit.transform;

            //Validando la frecuencia de disparo
            if (temporizador >= frecuenciaDisparo)
            {
                //reiniciando el temporizador
                temporizador = 0;

                // Crear instancia de la bala
                GameObject balaObjeto = Instantiate(balaPegar, posicionPistola.transform.position, posicionPistola.transform.rotation) as GameObject;
                //Obteniendo el rigid body
                Rigidbody balaRigid = balaObjeto.GetComponent<Rigidbody>();
                //Añadiendo la velocidad
                balaRigid.velocity = fuerzaBala * posicionPistola.transform.forward;


                // Change the clip to the firing clip and play it.
                //m_ShootingAudio.clip = m_FireClip;
                //m_ShootingAudio.Play ();

                //regresando la variable pega
                pega = false;
            }


            return NodeState.NodeStates.SUCCESS;

        }
        else
        {
            return NodeState.NodeStates.FAILURE;
        }
    }

	// Encontrar al enemigo más cercano

	GameObject EncontrarEnemigoCercano(string tagEnemigos) {
		//variable para guardar los enemigos
		GameObject[] enemigos;
		//obteniendo los enemigos que tengan cierto tag
		enemigos = GameObject.FindGameObjectsWithTag(tagEnemigos);
		//variable para guardar el nuevo enemigo a seguir
		GameObject enemigoCercano = null;
		//limpiando la  distancia con un numero muy grande 
		float distancia = Mathf.Infinity;
		//obtener la posicion de este personaje
		Vector3 posicion = transform.position;
		//ciclo para cada enemigo encontrado
		foreach (GameObject enemigo in enemigos) {
			//obteniendo la diferencia de distancia
			Vector3 diferenciaDistancia = enemigo.transform.position - posicion;
			//obteniendo la distancia con el enemigo (magnitud)
			float distanciaEnemigo = diferenciaDistancia.sqrMagnitude;

			//validando para que se obtenga el enemigo con menor distancia
			if (distanciaEnemigo < distancia) {
				enemigoCercano = enemigo;
				distancia = distanciaEnemigo;
			}
		}
		return enemigoCercano;
	}
}
