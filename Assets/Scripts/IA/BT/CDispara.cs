﻿using UnityEngine;
using System.Collections;

public class CDispara : MonoBehaviour
{

	//Instancia del script 
	EnemyTree enemigoScript;


	void Start(){

		if (enemigoScript == null) {
			enemigoScript = GetComponentInParent<EnemyTree>();
		}

	}

    //Mientras el player esta dentro del collider entonces dispara
    void OnTriggerStay(Collider other)
    {
        //comparando si es el jugador
		if ( other.gameObject.CompareTag("Player") == true || other.gameObject.CompareTag("Enemigo") == true )
        {

			enemigoScript.dispara = true;
        }
    }

    //Si se salio del collider entonces es falso
    void OnTriggerExit(Collider other)
    {
        //comparando si es el jugador
		if (other.gameObject.CompareTag("Player") == true || other.gameObject.CompareTag("Enemigo") == true )
        {
			enemigoScript.dispara = false;
        }
    }

}