﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ActionNode : Node {

    /* Method signature for the action. */
    public delegate NodeState.NodeStates ActionNodeDelegate();

    /* The delegate that is called to evaluate this node */
    private ActionNodeDelegate m_action;

    /* Because this node contains no logic itself,
     * the logic must be passed in in the form of 
     * a delegate. As the signature states, the action
     * needs to return a NodeStates enum */
    public ActionNode(ActionNodeDelegate action)
    {
        m_action = action;
    }

    /* Evaluates the node using the passed in delegate and 
     * reports the resulting state as appropriate */
    public override NodeState.NodeStates Evaluate()
    {
        switch (m_action())
        {
            case NodeState.NodeStates.SUCCESS:
                m_nodeState = NodeState.NodeStates.SUCCESS;
                return m_nodeState;
            case NodeState.NodeStates.FAILURE:
                m_nodeState = NodeState.NodeStates.FAILURE;
                return m_nodeState;
            case NodeState.NodeStates.RUNNING:
                m_nodeState = NodeState.NodeStates.RUNNING;
                return m_nodeState;
            default:
                m_nodeState = NodeState.NodeStates.FAILURE;
                return m_nodeState;
        }
    }
}
