﻿using UnityEngine;
using System.Collections;

public class ScriptAgente : MonoBehaviour {

	//Personaje al que va a seguir
	public Transform target;

	//NavMesh que ocupa el agente
	NavMeshAgent agent;


	// Use this for initialization
	void Start () {
		//Inicializando el agente
		agent = GetComponent < NavMeshAgent> ();
	}
	
	// Update is called once per frame
	void Update () {

		//Definiendo el destino para que vaya hacia el personaje
		agent.SetDestination (target.position);
	
	}
}
