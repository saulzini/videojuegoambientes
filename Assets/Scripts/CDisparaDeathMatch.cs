﻿using UnityEngine;
using System.Collections;

public class CDisparaDeathMatch: MonoBehaviour
{

	//Instancia del script 
	EnemigoDeathMatch enemigoScript;

	void Awake (){


		if (enemigoScript == null) {
			enemigoScript = GetComponentInParent<EnemigoDeathMatch>();
		}

	}



	void Start(){

		if (enemigoScript == null) {
			enemigoScript = GetComponentInParent<EnemigoDeathMatch>();
		}


	}

    //Mientras el player esta dentro del collider entonces dispara
    void OnTriggerStay(Collider other)
    {
		
        //comparando si es el jugador
		if ( other.gameObject.tag == enemigoScript.EquipoEnemigo == true )
        {
				enemigoScript.dispara = true;
        }
    }

    //Si se salio del collider entonces es falso
    void OnTriggerExit(Collider other)
    {
		//comparando si es el jugador
		if ( other.gameObject.tag == enemigoScript.EquipoEnemigo == true)
		{
			enemigoScript.dispara = false;
		}
    }

}