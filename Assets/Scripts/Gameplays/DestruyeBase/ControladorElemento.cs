﻿using UnityEngine;
using System;

[Serializable]
public class ControladorElemento {
                            
    [HideInInspector] public int elementoNum;
    [HideInInspector] public GameObject instancia;
    [HideInInspector] public VidaDestruyeBase vida;
    [HideInInspector] public EnemigoDestruyeBase enemigoDB;
    public Color color;
    public string tag;
    public string tagEnemigo;

    public void Setup(GameObject miBase, GameObject baseEnemiga)
    {
        instancia.GetComponent<Renderer>().material.color = color;
        vida = instancia.GetComponent<VidaDestruyeBase>();
        instancia.tag = tag;
        enemigoDB = instancia.GetComponent<EnemigoDestruyeBase>();
        if(enemigoDB != null)
        {
            enemigoDB.EquipoAliado = tag;
            enemigoDB.EquipoEnemigo = tagEnemigo;
            enemigoDB.miBase = miBase;
            enemigoDB.baseEnemiga = baseEnemiga;
            enemigoDB.estoyEnBaseEnemiga = false;
            enemigoDB.estoyEnMiBase = true;
            
        }
    }

    // Poner el elemento en su estado inicial
    public void Reinicio(Transform puntoSpawn)
    {
        instancia.transform.position = puntoSpawn.position;
        instancia.transform.rotation = puntoSpawn.rotation;

        if(vida != null)
            vida.vidaActual = vida.vidaInicial;

        if(enemigoDB != null)
        {
            enemigoDB.estoyEnBaseEnemiga = false;
            enemigoDB.estoyEnMiBase = true;
        }

        instancia.SetActive(false);
        instancia.SetActive(true);
    }

    public void Habilitar()
    {
        if(enemigoDB != null)
            enemigoDB.enabled = true;
    }

    public void Deshabilitar()
    {
        if (enemigoDB != null)
            enemigoDB.enabled = false;
    }

}
