﻿using UnityEngine;
using System.Collections;

public class CDisparaDB : MonoBehaviour {

    //Instancia del script 
    EnemigoDestruyeBase enemigoScript;

    void Awake()
    {
        if (enemigoScript == null)
            enemigoScript = GetComponentInParent<EnemigoDestruyeBase>();
    }

    //Mientras el player esta dentro del collider entonces dispara
    void OnTriggerStay(Collider other)
    {
        //comparando si del equipo enemigo
        if (other.gameObject.CompareTag(enemigoScript.EquipoEnemigo))
        {
            enemigoScript.dispara = true;
        }
    }

    //Si se salio del collider entonces es falso
    void OnTriggerExit(Collider other)
    {
        //comparando si es del equipo enemigo
        if (other.gameObject.CompareTag(enemigoScript.EquipoEnemigo))
        {
            enemigoScript.dispara = false;
        }
    }
}
