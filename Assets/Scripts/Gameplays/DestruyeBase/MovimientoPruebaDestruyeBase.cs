﻿using UnityEngine;
using System.Collections;

public class MovimientoPruebaDestruyeBase : MonoBehaviour {

    float speed = 5f;

    public GameObject bala;
    private GameObject bala1;
    public Transform balaPos;
    private GameObject bomba;

    // Use this for initialization
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        tieneBomba();
        if (Input.GetKey(KeyCode.RightArrow))
        {
            transform.position += Vector3.right * speed * Time.deltaTime;
        }
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            transform.position += Vector3.left * speed * Time.deltaTime;
        }
        if (Input.GetKey(KeyCode.UpArrow))
        {
            transform.position += Vector3.forward * speed * Time.deltaTime;
        }
        if (Input.GetKey(KeyCode.DownArrow))
        {
            transform.position += Vector3.back * speed * Time.deltaTime;
        }
        if (Input.GetKeyDown(KeyCode.Space))
        {
            Dispara();
        }
        if(bomba != null)
        {
            if (Input.GetKeyDown(KeyCode.A))
            {
                DisparaBomba();
            }
        }
    }

    void tieneBomba()
    {
        bomba = null;
        //Encontrar si tiene una bomba como hijo
        foreach (Transform tr in transform)
        {
            if (tr.tag == "Bomba")
               bomba = tr.gameObject;
        }
    }

    void Dispara()
    {
        // Create the Bullet from the Bullet Prefab
        bala1 = (GameObject)Instantiate(bala, balaPos.position, balaPos.rotation);

        // Add velocity to the bullet
        bala1.GetComponent<Rigidbody>().velocity = bala1.transform.forward * 10;
    }

    void DisparaBomba()
    {
        //Soltar la bomba con A
        bomba.transform.position += Vector3.forward * 100 * Time.deltaTime;
        bomba.transform.parent = null;
    }

}
