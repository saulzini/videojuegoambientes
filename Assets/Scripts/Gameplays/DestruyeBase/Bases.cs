﻿using UnityEngine;
using System.Collections;

public class Bases : MonoBehaviour {

    //creando instancia de enemigo
    EnemigoDestruyeBase enemigoScript;

    //Mientras el player esta dentro del collider entonces pega
    void OnTriggerStay(Collider other)
    {
        enemigoScript = null;
        //comparando si del equipo enemigo
        if (other.gameObject.CompareTag("EquipoAzul") || other.gameObject.CompareTag("EquipoRojo"))
        {
            enemigoScript = other.gameObject.GetComponent<EnemigoDestruyeBase>();
            if (gameObject.CompareTag("Base" + enemigoScript.EquipoAliado))
                enemigoScript.estoyEnMiBase = true;
            else if (gameObject.CompareTag("Base" + enemigoScript.EquipoEnemigo))
                enemigoScript.estoyEnBaseEnemiga = true;
        }
    }

    //Si se salio del collider entonces es falso
    void OnTriggerExit(Collider other)
    {
        enemigoScript = null;
        if (other.gameObject.CompareTag("EquipoAzul") || other.gameObject.CompareTag("EquipoRojo"))
        {
            enemigoScript = other.gameObject.GetComponent<EnemigoDestruyeBase>();
            if (gameObject.CompareTag("Base" + enemigoScript.EquipoAliado))
                enemigoScript.estoyEnMiBase = false;
            else if (gameObject.CompareTag("Base" + enemigoScript.EquipoEnemigo))
                enemigoScript.estoyEnBaseEnemiga = false;
        }
    }

    public void DestruirBase()
    {
        print("Base destruida");
        //particle system
        gameObject.SetActive(false);
    }
	
}
