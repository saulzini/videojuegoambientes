﻿using UnityEngine;
using System.Collections;

public class Bomba : MonoBehaviour {

    public float tiempoParaExplotar = 100f;
    private float tiempo;
    private float tiempoActual;
    private bool bombaActivada = false;
    public static bool estaEnBase = false;
    private Bases scriptBaseAzul;
    private Bases scriptBaseRojo;

    //creando instancia de enemigo
    EnemigoDestruyeBase[] enemigoScript;

    void Start()
    {
        if (enemigoScript == null)
            enemigoScript = FindObjectsOfType<EnemigoDestruyeBase>();
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "EquipoRojo" || other.tag == "EquipoAzul" || other.tag == "Player")
        {
            //Iniciar timer para explosion
            //Asegurar que el timer solo inicie una vez que tomas la bomba hasta que explota
            if (bombaActivada == false)
            {
                bombaActivada = true;
                tiempoActual = Time.time;
            }
            if (gameObject.transform.parent == null)
            {
                gameObject.transform.parent = other.transform;
            }
        }

        if (other.tag == "BaseEquipoAzul")
        {
            estaEnBase = true;
            enviarInfoAScript(other);
            scriptBaseAzul = other.GetComponent<Bases>();
        }

        if(other.tag == "BaseEquipoRojo")
        {
            estaEnBase = true;
            enviarInfoAScript(other);
            scriptBaseRojo = other.GetComponent<Bases>();
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.tag == "BaseEquipoAzul")
        {
            estaEnBase = false;
            enviarInfoAScript(other);
            scriptBaseAzul = null;
        }
        else if(other.tag == "BaseEquipoRojo")
        {
            estaEnBase = false;
            enviarInfoAScript(other);
            scriptBaseRojo = null;
        }
    }

    void Update()
    {
        
        if(bombaActivada)
        {
            //print("tiempo restante: " + ((tiempoActual + tiempoParaExplotar) - Time.time));
            //Después de que termine el tiempo para explotar, explota la bomba
            if (Time.time >= (tiempoActual + tiempoParaExplotar))
            {
                explotar();
                //Despues de explotar, regresa la bomba a sus valores iniciales
                restablecer();
            }
        }
    }

    void explotar()
    {
        print("exploto");
        
        gameObject.SetActive(false);
        gameObject.transform.parent = null;
        if (estaEnBase)
        {
            if (scriptBaseAzul != null)
                scriptBaseAzul.DestruirBase();
            else if (scriptBaseRojo != null)
                scriptBaseRojo.DestruirBase();
        }
    }

    public void restablecer()
    {
        bombaActivada = false;
        estaEnBase = false;
        scriptBaseAzul = null;
        scriptBaseRojo = null;
    }

    void enviarInfoAScript(Collider other)
    {
        foreach(EnemigoDestruyeBase script in enemigoScript)
        {
            if (estaEnBase)
            {
                if (other.CompareTag("Base" + script.EquipoAliado))
                    script.bombaEnMiBase = true;
                else if (other.CompareTag("Base" + script.EquipoEnemigo))
                    script.bombaEnBaseEnemiga = true;
            }
            else
            {
                if (other.CompareTag("Base" + script.EquipoAliado))
                    script.bombaEnMiBase = false;
                else if (other.CompareTag("Base" + script.EquipoEnemigo))
                    script.bombaEnBaseEnemiga = false;
            }
        }
    }

}
