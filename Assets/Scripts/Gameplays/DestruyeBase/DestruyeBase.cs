﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class DestruyeBase : MonoBehaviour
{

    public int numRonda = 0;
    public int puntosParaGanar = 3;
    [HideInInspector]
    public int puntosRojo = 0;
    [HideInInspector]
    public int puntosAzul = 0;
    [HideInInspector]
    public bool hayGanadorJuego = false;
    [HideInInspector]
    public string ganadorRonda;
    [HideInInspector]
    public string ganadorJuego;

    public Text msg;

    public GameObject prefabEquipoAzul;
    public GameObject prefabEquipoRojo;
    public GameObject prefabJugadorAzul;
    public GameObject prefabJugadorRojo;
    public GameObject prefabBomba;

    public ControladorElemento[] equipoAzul;
    public ControladorElemento[] equipoRojo;
    public GameObject baseAzul;
    public GameObject baseRoja;
    public GameObject[] bombas;
    [HideInInspector]
    public Bomba[] bombaScript;

    public Transform[] puntoSpawnEquipoAzul;
    public Transform[] puntoSpawnEquipoRojo;
    public Transform[] puntoSpawnBomba;

    public float delayInicio = 3f;  //Delay entre el la fase de inicioRonda y la fase juegoRonda
    public float delayFinal = 3f;    //Delay entre la fase de juegoRonda y el finRonda
    private WaitForSeconds tiempoIni;
    private WaitForSeconds tiempoFin;

    // Use this for initialization
    void Start()
    {

        tiempoIni = new WaitForSeconds(delayInicio);
        tiempoFin = new WaitForSeconds(delayFinal);

        //Crear bombas
        bombas = new GameObject[puntoSpawnBomba.Length];
        bombaScript = new Bomba[puntoSpawnBomba.Length];
        crearBombas();

        // Crear los equipos
        spawnEquipos();

        StartCoroutine(Juego());
    }

    // Se llama al inicio de cada ronda y ejecuta cada fase de cada ronda hasta que el jugador muera.
    private IEnumerator Juego()
    {
        // Iniciar la corutina 'inicioRonda'.
        yield return StartCoroutine(inicioRonda());

        // Ya que termine la corutina 'inicioRonda' iniciar la corutina 'juegoRonda'.
        yield return StartCoroutine(juegoRonda());

        // Ya que termina la corutina 'inicioRonda' inicia 'finRonda'.
        yield return StartCoroutine(finRonda());

        continuar();

    }

    private IEnumerator inicioRonda()
    {
        print("inicio ronda");
        condicionesInicio();

        // Esperar un tiempo para empezar la fase de juego.
        yield return tiempoIni;
    }

    private IEnumerator juegoRonda()
    {
        print("juegoRonda");
        msg.text = "";
        habilitarEquipos();
        //Si no se ha terminado la ronda...
        while (!terminaRonda())
        {
            //Revisar si murio algún integrante del equipo. Si murió revivirlo después de un tiempo.
            for (int i = 0; i < equipoRojo.Length; i++)
            {
                if (!equipoRojo[i].instancia.activeSelf)
                {
                    yield return tiempoFin;
                    equipoRojo[i].Reinicio(puntoSpawnEquipoRojo[i]);
                }
                else if (!equipoAzul[i].instancia.activeSelf)
                {
                    yield return tiempoFin;
                    equipoAzul[i].Reinicio(puntoSpawnEquipoAzul[i]);
                }
            }
            //Reactivar las bombas que ya hayan explotado
            reiniciarBombas();
            // ... regresar en el siguiente frame.
            yield return null;
        }

        finBombas();
    }

    private IEnumerator finRonda()
    {
        print("Fin ronda");
        //Obtener el ganador de la ronda
        ganadorRonda = obtenerGanadorRonda();

        if (ganadorRonda == "Rojo")
            puntosRojo++;
        else if (ganadorRonda == "Azul")
            puntosAzul++;

        msg.text = ganadorRonda;
        ganadorJuego = obtenerGanadorJuego();

        if (hayGanadorJuego)
            msg.text = ganadorJuego;

        yield return tiempoFin;
    }

    //Inicializar las variables necesarias para el nivel
    public void condicionesInicio()
    {
        numRonda++;
        reiniciaEquipos();
        reiniciaBases();
        finBombas();
        deshabilitarEquipos();
        //actualizar numero de ronda en el HUD
        msg.text = "Ronda " + numRonda;
    }

    public void spawnEquipos()
    {
        //Jugador equipo Azul
        equipoAzul[0].instancia = Instantiate(prefabJugadorAzul, puntoSpawnEquipoAzul[0].position, puntoSpawnEquipoAzul[0].rotation) as GameObject;
        equipoAzul[0].Setup(baseAzul, baseRoja);
        //Crear equipo Azul
        for (int i = 1; i < equipoAzul.Length; i++)
        {
            equipoAzul[i].instancia = Instantiate(prefabEquipoAzul, puntoSpawnEquipoAzul[i].position, puntoSpawnEquipoAzul[i].rotation) as GameObject;
            equipoAzul[i].elementoNum = i + 1;
            equipoAzul[i].Setup(baseAzul, baseRoja);
        }

        //Jugador equipo Rojo
        equipoRojo[0].instancia = Instantiate(prefabJugadorRojo, puntoSpawnEquipoRojo[0].position, puntoSpawnEquipoRojo[0].rotation) as GameObject;
        equipoRojo[0].Setup(baseRoja, baseAzul);
        //Crear equipo Rojo
        for (int i = 1; i < equipoRojo.Length; i++)
        {
            equipoRojo[i].instancia = Instantiate(prefabEquipoRojo, puntoSpawnEquipoRojo[i].position, puntoSpawnEquipoRojo[i].rotation) as GameObject;
            equipoRojo[i].elementoNum = i + 1;
            equipoRojo[i].Setup(baseRoja, baseAzul);
        }

    }

    public void reiniciaEquipos()
    {
        for (int i = 0; i < equipoRojo.Length; i++)
        {
            equipoRojo[i].Reinicio(puntoSpawnEquipoRojo[i]);
            equipoAzul[i].Reinicio(puntoSpawnEquipoAzul[i]);
        }
    }

    public void reiniciaBases()
    {
        //Reactivar las bases
        baseAzul.SetActive(true);
        baseRoja.SetActive(true);
    }

    public void crearBombas()
    {
        for (int i = 0; i < puntoSpawnBomba.Length; i++)
        {
            bombas[i] = Instantiate(prefabBomba, puntoSpawnBomba[i].position, puntoSpawnBomba[i].rotation) as GameObject;
            bombaScript[i] = bombas[i].GetComponent<Bomba>();
        }
    }

    public void reiniciarBombas()
    {
        for (int i = 0; i < puntoSpawnBomba.Length; i++)
        {
            if (!bombas[i].activeSelf)
            {
                bombas[i].transform.position = puntoSpawnBomba[i].position;
                bombas[i].transform.rotation = puntoSpawnBomba[i].rotation;
                bombaScript[i].restablecer();
                bombas[i].SetActive(true);
            }
        }
    }

    public void finBombas()
    {
        for (int i = 0; i < puntoSpawnBomba.Length; i++)
        {
            bombas[i].SetActive(false);
            bombas[i].transform.position = puntoSpawnBomba[i].position;
            bombas[i].transform.rotation = puntoSpawnBomba[i].rotation;
            bombaScript[i].restablecer();
        }
    }

    bool terminaRonda()
    {
        if ((baseAzul.activeSelf && !baseRoja.activeSelf) || (baseRoja.activeSelf && !baseAzul.activeSelf) || (!baseRoja.activeSelf && !baseAzul.activeSelf))
            return true;
        else
            return false;
    }

    string obtenerGanadorRonda()
    {
        if (baseAzul.activeSelf && !baseRoja.activeSelf)
            return "Azul";
        else if (baseRoja.activeSelf && !baseAzul.activeSelf)
            return "Rojo";
        else
            return "Empate";
    }

    string obtenerGanadorJuego()
    {
        if (puntosAzul == puntosParaGanar)
        {
            hayGanadorJuego = true;
            return "Gano Azul";
        }
        else if (puntosRojo == puntosParaGanar)
        {
            hayGanadorJuego = true;
            return "Gano Rojo";
        }
        else
        {
            hayGanadorJuego = false;
            return "";
        }
    }

    //Funcion para decidir que escena se va a cargar o que se va a hacer al final del la corutina juego
    public void continuar()
    {
        //Si no hay ganador empezar otra ronda
        if (!hayGanadorJuego)
        {
            StartCoroutine(Juego());
        }
        else
        {
            // Si hay ganador, mostrar el ganador y terminar el juego.
            destruyeEquipos();
        }
    }

    public void deshabilitarEquipos()
    {
        for (int i = 0; i < equipoRojo.Length; i++)
        {
            equipoRojo[i].Deshabilitar();
            equipoAzul[i].Deshabilitar();
        }
    }

    public void habilitarEquipos()
    {
        for (int i = 0; i < equipoRojo.Length; i++)
        {
            equipoRojo[i].Habilitar();
            equipoAzul[i].Habilitar();
        }
    }

    public void destruyeEquipos()
    {
        for (int i = 0; i < equipoRojo.Length; i++)
        {
            equipoRojo[i].instancia.SetActive(false);
            equipoAzul[i].instancia.SetActive(false);
        }
    }

}
