﻿using UnityEngine;
using System.Collections;

public class CPegaDB : MonoBehaviour {

    //creando instancia de enemigo
    EnemigoDestruyeBase enemigoScript;

    void Awake()
    {
        if (enemigoScript == null)
            enemigoScript = GetComponentInParent<EnemigoDestruyeBase>();
    }

    //Mientras el player esta dentro del collider entonces pega
    void OnTriggerStay(Collider other)
    {
        //comparando si del equipo enemigo
        if (other.gameObject.CompareTag(enemigoScript.EquipoEnemigo))
        {
            enemigoScript.pega = true;
        }
    }

    //Si se salio del collider entonces es falso
    void OnTriggerExit(Collider other)
    {
        //comparando si es del equipo enemigo
        if (other.gameObject.CompareTag(enemigoScript.EquipoEnemigo))
        {
            enemigoScript.pega = false;
        }
    }
}
