﻿using UnityEngine;
using System.Collections;

public class CPegarReyColina: MonoBehaviour {

	//creando instancia de enemigo
	EnemigoReyColina enemigoScript;



	void Awake(){



		if (enemigoScript == null) {
			enemigoScript = GetComponentInParent<EnemigoReyColina>();
		}


	}

	//Mientras el player esta dentro del collider entonces dispara
	void OnTriggerStay(Collider other)
	{

		//comparando si es el jugador
		if (other.gameObject.CompareTag(enemigoScript.EquipoEnemigo))
		{
			enemigoScript.pega = true;
		}
	}

	//Si se salio del collider entonces es falso
	void OnTriggerExit(Collider other)
	{
		//comparando si es el jugador
		if (other.gameObject.CompareTag(enemigoScript.EquipoEnemigo))
		{
			enemigoScript.pega = false;
		}
	}
}
