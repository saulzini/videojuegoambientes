﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class ReyColina : MonoBehaviour {

	public int numRonda = 0;

	public Text msg;

	public GameObject prefabIA;

	public GameObject jugador1;
	public GameObject jugador2;

	public GameObject[] integrantesRojo;
	public GameObject[] integrantesAzul;

	public GameObject baseEquipoRojo;
	public GameObject baseEquipoAzul;


	public float delayInicio = 5f;  //Delay entre el la fase de inicioRonda y la fase juegoRonda
	public float delayFinal = 5f;    //Delay entre la fase de juegoRonda y el finRonda
	private WaitForSeconds tiempoIni;       
	private WaitForSeconds tiempoFin;

	[HideInInspector] public string equipoRojo = "EquipoRojo";
	[HideInInspector] public string equipoAzul = "EquipoAzul";


	[HideInInspector] public bool quitarTiempoEquipoAzul;
	[HideInInspector] public bool quitarTiempoEquipoRojo;

	[HideInInspector] public bool puntoParaEquipoAzul;
	[HideInInspector] public bool puntoParaEquipoRojo;

	public int numeroIntegrantesEquipo = 5;

	private GameObject jugador;

	public Transform[] puntoSpawnEquipoRojo;
	public Transform[] puntoSpawnEquipoAzul;

	private Vida[] vidaEquipoRojo; //Script de vidas del equipo rojo
	private Vida[] vidaEquipoAzul; //Script de vidas del equipo azul


	public int rondasParaGanar = 3;
	[HideInInspector] public int rondasGanadasEquipoAzul = 0;
	[HideInInspector] public int rondasGanadasEquipoRojo = 0;

	public static string estadoJuego;

	public float tiempoEquipo = 10;
	static float tiempoEquipoRojo;
	static float tiempoEquipoAzul;

	private string minutosEquipoAzul;
	private string segundosEquipoAzul;

	private string minutosEquipoRojo;
	private string segundosEquipoRojo;

	public Text tiempoEquipoAzulText;
	public Text tiempoEquipoRojoText;


	// Inicializando
	void Start () {

		integrantesRojo = new GameObject[numeroIntegrantesEquipo];
		integrantesAzul = new GameObject[numeroIntegrantesEquipo];

		tiempoIni = new WaitForSeconds(delayInicio);
		tiempoFin = new WaitForSeconds(delayFinal);

		tiempoEquipoAzul = tiempoEquipo;
		tiempoEquipoRojo = tiempoEquipo;
	

		//maximo 5 integrantes por equipo
		if (numeroIntegrantesEquipo > 5) {
			numeroIntegrantesEquipo = 5;

		}

		StartCoroutine(Juego());
	}


	void Update()
	{
		if (quitarTiempoEquipoAzul == true) {
				minutosEquipoAzul = Mathf.Floor (tiempoEquipoAzul / 60).ToString ("00");
				segundosEquipoAzul = (tiempoEquipoAzul % 60).ToString ("00");

				tiempoEquipoAzulText.text = "Equipo azul " + minutosEquipoAzul + ":" + segundosEquipoAzul;
				tiempoEquipoAzul -= Time.deltaTime;
			} 
		if (quitarTiempoEquipoRojo == true) {
				minutosEquipoRojo = Mathf.Floor (tiempoEquipoRojo / 60).ToString ("00");
				segundosEquipoRojo = (tiempoEquipoRojo % 60).ToString ("00");

				tiempoEquipoRojoText.text = "Equipo rojo " + minutosEquipoRojo + ":" + segundosEquipoRojo;
				tiempoEquipoRojo -= Time.deltaTime;
			}
	
	}

	// Se llama al inicio de cada ronda y ejecuta cada fase de cada ronda hasta que el jugador muera.
	private IEnumerator Juego()
	{
		// Iniciar la corutina 'inicioRonda'.
		yield return StartCoroutine(inicioRonda());

		// Ya que termine la corutina 'inicioRonda' iniciar la corutina 'juegoRonda'.
		yield return StartCoroutine(juegoRonda());

		// Ya que termina la corutina 'inicioRonda' inicia 'finRonda'.
		yield return StartCoroutine(finRonda());

		continuar();
	}

	private IEnumerator inicioRonda()
	{
		print("inicio ronda");
		estadoJuego = "Inicio";

		condicionesInicio();

		// Esperar un tiempo para empezar la fase de juego.
		yield return tiempoIni;
	}

	void RevisaColina(){

		int contadorAzul=0;
		int contadorRojo=0;

		for (int i = 0; i < numeroIntegrantesEquipo; i++) {

			if (integrantesAzul [i] != null) {

				Vida scriptPersonajeAzul = integrantesAzul [i].GetComponent<Vida> ();
				if (scriptPersonajeAzul.estoyColina == true) {
					contadorAzul++;

				}
			}

			if (integrantesRojo [i] != null) {
				Vida scriptPersonajeRojo = integrantesRojo [i].GetComponent<Vida> ();


				if (scriptPersonajeRojo.estoyColina == true) {
			
					contadorRojo++;
				}
			}
		
		}



		if (contadorAzul > 0 && contadorRojo == 0) {
			quitarTiempoEquipoAzul = true;
		
		} else if (contadorRojo > 0 && contadorAzul == 0) {
		
			quitarTiempoEquipoRojo = true;
		} else {
		
			quitarTiempoEquipoAzul = false;
			quitarTiempoEquipoRojo = false;	
		
		}



	}


	private IEnumerator juegoRonda()
	{
		print("juegoRonda");
		msg.text = "";
		//Si no se ha terminado la ronda...
		while (!terminaRonda())
		{
			RevisaColina ();

			for (int i = 0; i < numeroIntegrantesEquipo; i++) {

				if (i == 0) {
					if (integrantesAzul [i] == null) {
						integrantesAzul[i] = instanciandoPersonaje(false,equipoAzul,puntoSpawnEquipoAzul[i]);
					}
					if (integrantesRojo [i] == null) {
						integrantesRojo[i] =  instanciandoPersonaje(false,equipoRojo,puntoSpawnEquipoRojo[i]);

					}
				} 
				else {
					if (integrantesAzul [i] == null) {
						integrantesAzul[i] = instanciandoPersonaje(true,equipoAzul,puntoSpawnEquipoAzul[i]);

					}
					if (integrantesRojo [i] == null) {
						integrantesRojo[i] = instanciandoPersonaje(true,equipoRojo,puntoSpawnEquipoRojo[i]);

					}

				}

				yield return 100;
			}

			// ... regresar en el siguiente frame.
			yield return null;
		}
	}


	void reiniciarJuego(){
		
		tiempoEquipoAzul = tiempoEquipo;
		tiempoEquipoRojo = tiempoEquipo;

		puntoParaEquipoAzul = false;
		puntoParaEquipoRojo = false;

		for (int i = 0; i < numeroIntegrantesEquipo; i++) {
			Destroy(integrantesAzul[i]);
			Destroy(integrantesRojo[i]);
		}


		integrantesRojo = new GameObject[numeroIntegrantesEquipo];
		integrantesAzul = new GameObject[numeroIntegrantesEquipo];

	}

	private IEnumerator finRonda()
	{
		print("Fin ronda");
		quitarTiempoEquipoAzul = false;
		quitarTiempoEquipoRojo = false;
		tiempoEquipoRojo = 0;
		tiempoEquipoAzul = 0;
		reiniciarJuego ();
		yield return tiempoFin;
	}

	//Inicializar las variables necesarias para el nivel
	public virtual void condicionesInicio()
	{
		msg.text = "";
		tiempoEquipoAzulText.text = "";
		tiempoEquipoRojoText.text = "";

		instanciandoEquipos ();

		numRonda++;

		//actualizar numero de ronda en el HUD
		msg.text = "Ronda " + numRonda;
	}

	bool terminaRonda()
	{
		int numeroIntegrantesRojo = GameObject.FindGameObjectsWithTag (equipoRojo).Length;
		int numeroIntegrantesAzul = GameObject.FindGameObjectsWithTag (equipoAzul).Length;

		if (tiempoEquipoAzul <= 0) {
			rondasGanadasEquipoAzul += 1;
			despliegaGanador (equipoAzul);
			return true;
		} else if (tiempoEquipoRojo <= 0) {
			rondasGanadasEquipoRojo += 1;
			despliegaGanador (equipoRojo);
			return true;
		}else
			return false;
	}


	//Funcion para decidir que escena se va a cargar o que se va a hacer al final del la corutina juego
	public virtual void continuar()
	{
		//Si nignuno del equipo ha llegado al numero de rondas para ganar entonces ir a la siguiente ronda
		if (rondasParaGanar > rondasGanadasEquipoAzul && rondasParaGanar > rondasGanadasEquipoRojo) {
			StartCoroutine (Juego ());
		} 
		else {
			//Obtener ganador
			if (rondasGanadasEquipoAzul > rondasGanadasEquipoRojo) {
				msg.text = "El ganador del juego fue el equipo Azul";

			} 
			else {
				msg.text  = "El ganador del juego fue el equipo Rojo";

			}
		}
	}

	public void instanciandoEquipos(){
		for (int i = 0; i < numeroIntegrantesEquipo; i++) {

			if (i == 0) {
				integrantesAzul[i]	= instanciandoPersonaje(false,equipoAzul,puntoSpawnEquipoAzul[i] );
				integrantesRojo[i]  = instanciandoPersonaje(false,equipoRojo,puntoSpawnEquipoRojo[i]);

			} else {

				integrantesAzul[i] = instanciandoPersonaje(true,equipoAzul,puntoSpawnEquipoAzul[i]);
				integrantesRojo[i] = instanciandoPersonaje(true,equipoRojo,puntoSpawnEquipoRojo[i]);
			}

		}

	}


	//metodo para instanciar a los equipos 
	GameObject instanciandoPersonaje( bool IA , string equipo , Transform ubicacion ){
		GameObject gameObjectInstanciado;
		if (IA == true) {
			if (equipo == equipoAzul) {

				//instanciando azules
				GameObject integranteAzul = Instantiate (prefabIA, ubicacion.position, Quaternion.identity) as GameObject; //equipo azul
				integranteAzul.tag = equipoAzul;
				integranteAzul.GetComponent<Renderer> ().material.color = Color.blue;
				//asignando como enemigo el equipo correspondiente
				EnemigoReyColina scriptIntegranteAzul = integranteAzul.GetComponent<EnemigoReyColina>();
				scriptIntegranteAzul.EquipoEnemigo = equipoRojo;
				scriptIntegranteAzul.EquipoAliado = equipoAzul;

				gameObjectInstanciado = integranteAzul;

			} else {
				//instanciando rojos
				GameObject integranteRojo = Instantiate (prefabIA, ubicacion.position, Quaternion.identity) as GameObject; //equipo rojo
				integranteRojo.tag = equipoRojo;
				integranteRojo.GetComponent<Renderer> ().material.color = Color.red; 
				//asignando como enemigo el equipo correspondiente
				EnemigoReyColina scriptIntegranteRojo = integranteRojo.GetComponent<EnemigoReyColina>();
				scriptIntegranteRojo.EquipoEnemigo = equipoAzul;
				scriptIntegranteRojo.EquipoAliado = equipoRojo;

				gameObjectInstanciado = integranteRojo;
			}
		} 
		else {
			if (equipo == equipoAzul) {

				//instanciando personajes
				GameObject personajeAzul = Instantiate(jugador1 , ubicacion.position,Quaternion.identity) as GameObject; //equipo azul
				personajeAzul.tag = equipoAzul;
				personajeAzul.GetComponent<Renderer> ().material.color = Color.blue; 

				gameObjectInstanciado = personajeAzul;
			} else {

				//instanciando rojos
				GameObject personajeRojo = Instantiate(jugador2, ubicacion.position,Quaternion.identity) as GameObject; //equipo rojo
				personajeRojo.tag = equipoRojo;
				personajeRojo.GetComponent<Renderer> ().material.color = Color.red; 

				gameObjectInstanciado = personajeRojo;
			}

		}
		return gameObjectInstanciado;

	}



	void despliegaGanador(string ganador){
		msg.text = "El ganador de la ronda fue: " + ganador;

	}

}
