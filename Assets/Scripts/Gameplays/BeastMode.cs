﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class BeastMode : MonoBehaviour {

    public int numEnemigosMin = 1; //Cantidad mínima de enemigos a generar.
    public int numEnemigosMax = 20; //Cantidad máxima de enemigos a generar.
    public float tiempoMin = 1; //Duración mínima intervalos de tiempo.
    public float tiempoMax = 2; //Duración máxima intervalos de tiempo.
    public int intervaloAumentoVida = 5; //Número de ciclos para aumentar la vida de los enemigos.
    //public Text msg;
    public float vidaInicialEnemigos = 10;
    public GameObject prefabEnemigo;
    public GameObject prefabJugador;

    private GameObject jugador;

    public Transform[] puntosSpawnEnemigos;
    public Transform[] puntosSpawnJugador;

    private VidaEnemigosBM vidaEnemigo; //Script de vida del enemigo
    private Vida vidaJugador; //Script de vida del jugador

    public float aumentoVida = 10f; //Cantidad de vida que se le agrega al enemigo

    public float delayInicio = 3f;  //Delay entre el la fase de inicioRonda y la fase juegoRonda
    public float delayFinal = 3f;    //Delay entre la fase de juegoRonda y el finRonda
    private WaitForSeconds tiempoIni;       
    private WaitForSeconds tiempoFin;

    // Use this for initialization
    void Start () {

        tiempoIni = new WaitForSeconds(delayInicio);
        tiempoFin = new WaitForSeconds(delayFinal);

        vidaEnemigo = prefabEnemigo.GetComponent<VidaEnemigosBM>();
        vidaEnemigo.vidaInicial = vidaInicialEnemigos;
        
        // Crear la instancia del jugador
        jugador = Spawn(prefabJugador, puntosSpawnJugador);
        vidaJugador = jugador.GetComponent<Vida>();

        StartCoroutine(Juego());
    }

    // Se llama al inicio de cada ronda y ejecuta cada fase de cada ronda hasta que el jugador muera.
    private IEnumerator Juego()
    {
        // Iniciar la corutina 'inicioRonda'.
        yield return StartCoroutine(inicioRonda());

        // Ya que termine la corutina 'inicioRonda' iniciar la corutina 'juegoRonda'.
        yield return StartCoroutine(juegoRonda());

        // Ya que termina la corutina 'inicioRonda' inicia 'finRonda'.
        yield return StartCoroutine(finRonda());

        continuar();
        
    }

    private IEnumerator inicioRonda()
    {
        print("inicio ronda");
        condicionesInicio();

        // Esperar un tiempo para empezar la fase de juego.
        yield return tiempoIni;
    }

    private IEnumerator juegoRonda()
    {
        float tiempoAleatorio;
        int enemigosAleatorio;
        int numCiclos = 0;
        print("juegoRonda");
        //msg.text = "";
        //Si no se ha terminado el juego...
        while (!terminaRonda())
        {
            numCiclos++;
            //Acciones que se van a ejecutar durante la ronda
            //En beast mode va creando una cantidad aleatoria de enemigos con respecto al tiempo y termina hasta que el jugador muere. Cada 5 ciclos de generar enemigos se les aumentara la vida
            enemigosAleatorio = Random.Range(numEnemigosMin, numEnemigosMax);       
            tiempoAleatorio = Random.Range(tiempoMin,tiempoMax);
            for (int i = 0; i < enemigosAleatorio; i++)
            {
                Spawn(prefabEnemigo, puntosSpawnEnemigos);
                yield return new WaitForSeconds(3f);
            }
            yield return new WaitForSeconds(tiempoAleatorio);
            print("Num ciclos");
            print(numCiclos);
            if(numCiclos == intervaloAumentoVida)
            {
                numCiclos = 0;
                vidaEnemigo.vidaInicial += aumentoVida;
            }
            // ... regresar en el siguiente frame.
            yield return null;
        }
    }

    private IEnumerator finRonda()
    {
        print("Fin ronda");
        yield return tiempoFin;
    }

    private GameObject Spawn(GameObject prefab, Transform[] puntos)
    {
        //Encontrar un punto random de spawn.
        int punto = Random.Range(0, puntos.Length);

        // Crear la instancia del prefab
        return (GameObject)Instantiate(prefab, puntos[punto].position, puntos[punto].rotation);
    }

    //Inicializar las variables necesarias para el nivel
    public virtual void condicionesInicio()
    {
        /*numRonda++;

        //actualizar numero de ronda en el HUD
        msg.text = "Ronda " + numRonda;*/
    }

    bool terminaRonda()
    {
        //Si el jugador ya no tiene vida termina el juego
        if (vidaJugador.vidaActual <= 0)
            return true;
        else
            return false;
        
    }

    //Funcion para decidir que escena se va a cargar o que se va a hacer al final del la corutina juego
    public virtual void continuar()
    {
        //Si el jugador sigue vivo iniciar la siguiente ronda
        if (vidaJugador.vidaActual >= 0)
        {
            StartCoroutine(Juego());
        }
        /*else
        {
            // Si el jugador murio cargar menu con puntuación.
        }*/
    }

}
