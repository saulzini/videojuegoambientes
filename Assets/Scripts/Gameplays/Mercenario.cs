﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Mercenario : MonoBehaviour {

    public int numRonda = 0;
    public int numEnemigosTotal = 20;
    public float tiempoInicial = 100f;
    public float tiempoMin = 1; //Duración mínima intervalos de tiempo.
    public float tiempoMax = 2; //Duración máxima intervalos de tiempo.
    public static float tiempoActual;
    private int enemigosToSpawn;
    private string minutos;//para imprimir el tiempo.
    private string segundos; //para imprimir el tiempo.
    public Text tiempo;
    public Text fin;

    public GameObject prefabEnemigo;
    public GameObject prefabJugador;
    private GameObject jugador;

    public Transform[] puntosSpawnEnemigos;
    public Transform[] puntosSpawnJugador;

    public float delayInicio = 3f;  //Delay entre el la fase de inicioRonda y la fase juegoRonda
    public float delayFinal = 3f;    //Delay entre la fase de juegoRonda y el finRonda
    private WaitForSeconds tiempoIni;       
    private WaitForSeconds tiempoFin;

    void Start () {

        tiempoActual = tiempoInicial;
        tiempoIni = new WaitForSeconds(delayInicio);
        tiempoFin = new WaitForSeconds(delayFinal);

        // Crear la instancia del jugador
        jugador = Spawn(prefabJugador, puntosSpawnJugador);

        StartCoroutine(Juego());
    }

    void Update()
    {
        minutos = Mathf.Floor(tiempoActual / 60).ToString("00");
        segundos = (tiempoActual % 60).ToString("00");

        tiempo.text = minutos + ":" + segundos;
        tiempoActual -= Time.deltaTime;
    }

    // Se llama al inicio de cada ronda y ejecuta cada fase de cada ronda hasta que el jugador muera.
    private IEnumerator Juego()
    {
        // Iniciar la corutina 'inicioRonda'.
        yield return StartCoroutine(inicioRonda());

        // Ya que termine la corutina 'inicioRonda' iniciar la corutina 'juegoRonda'.
        yield return StartCoroutine(juegoRonda());

        // Ya que termina la corutina 'inicioRonda' inicia 'finRonda'.
        yield return StartCoroutine(finRonda());

        continuar();
        
    }

    private IEnumerator inicioRonda()
    {
        print("inicio ronda");
        condicionesInicio();

        // Esperar un tiempo para empezar la fase de juego.
        yield return tiempoIni;
    }

    private IEnumerator juegoRonda()
    {
        int randomNum;
        float tiempoAleatorio;
        print("juegoRonda");
        //Si no se ha terminado la ronda...
        while (!terminaRonda())
        {
            tiempoAleatorio = Random.Range(tiempoMin, tiempoMax);
            //Acciones que se van a ejecutar durante la ronda
            //En mercenario se creara un numero total de enemigos en un tiempo aleatorio y termina cuando se acaba el tiempo
            if (enemigosToSpawn != 0)
            {
                randomNum = Random.Range(1, enemigosToSpawn / 2);
                enemigosToSpawn -= randomNum;
                for (int i = 0; i < randomNum; i++)
                {
                    Spawn(prefabEnemigo, puntosSpawnEnemigos);
                    yield return new WaitForSeconds(2f);
                }
                yield return new WaitForSeconds(tiempoAleatorio);
            }
            // ... regresar en el siguiente frame.
            yield return null;
        }
    }

    private IEnumerator finRonda()
    {
        print("Fin ronda");
        fin.text = "Game Over";
        yield return tiempoFin;
    }

    private GameObject Spawn(GameObject prefab, Transform[] puntos)
    {
        //Encontrar un punto random de spawn.
        int punto = Random.Range(0, puntos.Length);

        // Crear la instancia del prefab
        return (GameObject)Instantiate(prefab, puntos[punto].position, puntos[punto].rotation);
    }

    //Inicializar las variables necesarias para el nivel
    public virtual void condicionesInicio()
    {
        fin.text = "";
        enemigosToSpawn = numEnemigosTotal;
    }

    bool terminaRonda()
    {
        //Si el jugador ya no tiene tiempo termina
        if (tiempoActual <= 0)
            return true;
        else
            return false;
    }

    //Funcion para decidir que escena se va a cargar o que se va a hacer al final del la corutina juego
    public virtual void continuar()
    {
        //Al finalizar, cargar la escena con las puntuaciones
        
        
    }

}
