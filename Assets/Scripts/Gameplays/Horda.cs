﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Horda : MonoBehaviour {

    public int numRonda = 0;
    public int numPar = 10; //Número de enemigos que apreceran en las rondas par.
    public int numImpar = 20; //Número de enemigos que apareceran en las rondas impar.
    public int rondaUno = 10; //Número de enemigos que apareceran en la ronda 1.
    public int numEnemigosTotal;
    public int valorPuntuacion = 10; //Puntos que obtiene el jugador por ronda.
    private int enemigosToSpawn;
    public Text msg;

    public GameObject prefabEnemigo;
    public GameObject prefabJugador;
    private GameObject jugador;

    public Transform[] puntosSpawnEnemigos;
    public Transform[] puntosSpawnJugador;

    private Vida vidaEnemigo; //Script de vida del enemigo
    private Vida vidaJugador; //Script de vida del jugador
    public float vidaE;
    public float aumentoVida = 20f; //Cantidad de vida que se le agrega al enemigo

    public float delayInicio = 3f;  //Delay entre el la fase de inicioRonda y la fase juegoRonda
    public float delayFinal = 3f;    //Delay entre la fase de juegoRonda y el finRonda
    private WaitForSeconds tiempoIni;       
    private WaitForSeconds tiempoFin;

    // Use this for initialization
    void Start () {

        tiempoIni = new WaitForSeconds(delayInicio);
        tiempoFin = new WaitForSeconds(delayFinal);

        vidaEnemigo = prefabEnemigo.GetComponent<Vida>();

        // Crear la instancia del jugador
        jugador = Spawn(prefabJugador, puntosSpawnJugador);
        vidaJugador = jugador.GetComponent<Vida>();

        StartCoroutine(Juego());
    }

    // Se llama al inicio de cada ronda y ejecuta cada fase de cada ronda hasta que el jugador muera.
    private IEnumerator Juego()
    {
        // Iniciar la corutina 'inicioRonda'.
        yield return StartCoroutine(inicioRonda());

        // Ya que termine la corutina 'inicioRonda' iniciar la corutina 'juegoRonda'.
        yield return StartCoroutine(juegoRonda());

        // Ya que termina la corutina 'inicioRonda' inicia 'finRonda'.
        yield return StartCoroutine(finRonda());

        continuar();
        
    }

    private IEnumerator inicioRonda()
    {
        print("inicio ronda");
        condicionesInicio();

        // Esperar un tiempo para empezar la fase de juego.
        yield return tiempoIni;
    }

    private IEnumerator juegoRonda()
    {
        int randomNum;
        print("juegoRonda");
        msg.text = "";
        //Si no se ha terminado la ronda...
        while (!terminaRonda())
        {
            //Acciones que se van a ejecutar durante la ronda
            //En horda se va creando una cantidad aleatoria de enemigos de acuerdo a la cantidad 
            //determinada en enemigosToSpawn para cada nivel
            if (enemigosToSpawn != 0)
            {
                randomNum = Random.Range(1, enemigosToSpawn / 2);
                enemigosToSpawn -= randomNum;
                for (int i = 0; i < randomNum; i++)
                {
                    Spawn(prefabEnemigo, puntosSpawnEnemigos);
                    yield return new WaitForSeconds(1f);
                }
                //TO DO: tiempo random
                yield return new WaitForSeconds(5f);
            }
            // ... regresar en el siguiente frame.
            yield return null;
        }
    }

    private IEnumerator finRonda()
    {
        print("Fin ronda");
        //aumentar puntuación
        PuntuacionIndividual.puntos += valorPuntuacion;
        yield return tiempoFin;
    }

    private GameObject Spawn(GameObject prefab, Transform[] puntos)
    {
        //Encontrar un punto random de spawn.
        int punto = Random.Range(0, puntos.Length);

        // Crear la instancia del prefab
        return (GameObject)Instantiate(prefab, puntos[punto].position, puntos[punto].rotation);
    }

    //Inicializar las variables necesarias para el nivel
    public virtual void condicionesInicio()
    {
        vidaE = vidaEnemigo.vidaInicial;

        numRonda++;

        //Asignar numero total de enemigos para la ronda y vida para hacerlos más resistentes
        //En las rondas pares el numero total de enemigos será numPar y para los impares será numImpar
        //y la vida total de los enemigos ira incrementando cada ronda par
        //condicionesInicioRonda();
        if (numRonda > 1)
        {
            if ((numRonda % 2) == 0)
            {
                numEnemigosTotal = numPar;
                vidaEnemigo.vidaInicial = vidaE + aumentoVida;
            }
            else
            {
                numEnemigosTotal = numImpar;
            }
        }
        else
        {
            print("Ronda uno");
            numEnemigosTotal = rondaUno;
            //Opcional:
            vidaEnemigo.vidaInicial = 10;
            print(vidaEnemigo.vidaInicial);
        }
        enemigosToSpawn = numEnemigosTotal;
        //actualizar numero de ronda en el HUD
        msg.text = "Ronda " + numRonda;
    }

    bool terminaRonda()
    {
        int numEnemigosActual = GameObject.FindGameObjectsWithTag("Enemigo").Length;

        //Si el jugador ya no tiene vida termina y mientras el número de enemigos 
        //en la escena sea mayor a cero y/o todavía queden enemigos a crear
        if (vidaJugador.vidaActual <= 0)
            return true;
        else
        {
            if (enemigosToSpawn > 0)
            {
                return false;
            }
            else
            {
                if (numEnemigosActual > 0)
                    return false;
                else
                    return true;
            }
        }
    }

    //Funcion para decidir que escena se va a cargar o que se va a hacer al final del la corutina juego
    public virtual void continuar()
    {
        //Si el jugador sigue vivo iniciar la siguiente ronda
        if (vidaJugador.vidaActual >= 0)
        {
            StartCoroutine(Juego());
        }
        /*else
        {
            // Si el jugador murio cargar menu con puntuación.
        }*/
    }

}
