﻿using UnityEngine;
using System.Collections;

public class BaseEquipos : MonoBehaviour {

	[HideInInspector] public string equipoAzul  = "EquipoAzul";
	[HideInInspector] public string equipoRojo = "EquipoRojo";
	[HideInInspector] public string baseEquipoAzul = "BaseEquipoAzul";
	[HideInInspector] public string baseEquipoRojo = "BaseEquipoRojo";
	[HideInInspector] public string enemigo;
	[HideInInspector] public string miEquipo;
	[HideInInspector] public bool miBandera;


	// Use this for initialization
	void Start () {
		if (gameObject.tag == baseEquipoAzul) {
			miEquipo = equipoAzul;
			enemigo = equipoRojo;
		} 
		else if(gameObject.tag == baseEquipoRojo){
			miEquipo = equipoRojo;
			enemigo = equipoAzul;
		}

	}

	// Update is called once per frame
	void Update () {
		if (gameObject.tag == baseEquipoAzul)
			miBandera = CapturaBandera.banderaAzulCapturada;
		else if(gameObject.tag == baseEquipoRojo)
			miBandera = CapturaBandera.banderaRojoCapturada;
	}



	void OnTriggerStay(Collider other) {
		if (other.gameObject.CompareTag (miEquipo)) {
			bool bandera;

			if(other.gameObject.name == "EnemigoCapturaBandera(Clone)"){
				EnemigoCapturaBandera scriptEnemigo;
				scriptEnemigo = other.gameObject.GetComponent<EnemigoCapturaBandera> ();
				bandera = scriptEnemigo.tengoBanderaEnemiga;
			}
			else{
				VidaPersonajeCapturaBandera scriptPersonaje;
				scriptPersonaje =  other.gameObject.GetComponent<VidaPersonajeCapturaBandera> ();
				bandera = scriptPersonaje.tengoBanderaEnemiga;
			}

			if (bandera && !miBandera) {
				if(miEquipo == equipoRojo)
					CapturaBandera.puntoParaEquipoRojo = true;
				else if(miEquipo == equipoAzul)
					CapturaBandera.puntoParaEquipoAzul = true;
			}
		}
	}



}
