﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class CapturaBandera : MonoBehaviour {

	public int numRonda = 0;
	public Text msg;

	public GameObject prefabIA;

	public GameObject jugador1;
	public GameObject jugador2;

	public GameObject[] integrantesRojo;
	public GameObject[] integrantesAzul;


	public GameObject baseEquipoRojo;
	public GameObject baseEquipoAzul;

	public GameObject prefabBandera;

	public float delayInicio = 5f;  //Delay entre el la fase de inicioRonda y la fase juegoRonda
	public float delayFinal = 5f;    //Delay entre la fase de juegoRonda y el finRonda
	private WaitForSeconds tiempoIni;       
	private WaitForSeconds tiempoFin;

	string equipoRojo= "EquipoRojo";
	string equipoAzul= "EquipoAzul";

	string tagBanderaEquipoAzul= "BanderaAzul";
	string tagBanderaEquipoRojo= "BanderaRojo";


	[HideInInspector ] public static bool banderaRojoCapturada = false;
	[HideInInspector] public static bool banderaAzulCapturada = false;
	[HideInInspector] public static bool banderaRojoTirada = false;
	[HideInInspector] public static bool banderaAzulTirada = false;


	[HideInInspector] public static bool puntoParaEquipoRojo = false;
	[HideInInspector] public static bool puntoParaEquipoAzul = false;



	public int numeroIntegrantesEquipo = 5;

	private GameObject jugador;

	public Transform[] puntoSpawnEquipoRojo;
	public Transform[] puntoSpawnEquipoAzul;

	private Vida[] vidaEquipoRojo; //Script de vidas del equipo rojo
	private Vida[] vidaEquipoAzul; //Script de vidas del equipo azul


	public int rondasParaGanar = 3;
	[HideInInspector] public int rondasGanadasEquipoAzul = 0;
	[HideInInspector] public int rondasGanadasEquipoRojo = 0;


	public static Transform posicionInicialBanderaAzul;
	public static Transform posicionInicialBanderaRoja;

	GameObject BanderaRoja;
	GameObject BanderaAzul;



	// Inicializando
	void Start () {

		posicionInicialBanderaAzul = puntoSpawnEquipoAzul [0];
		posicionInicialBanderaRoja = puntoSpawnEquipoRojo [0];

		integrantesRojo = new GameObject[numeroIntegrantesEquipo];
		integrantesAzul = new GameObject[numeroIntegrantesEquipo];

		tiempoIni = new WaitForSeconds(delayInicio);
		tiempoFin = new WaitForSeconds(delayFinal);

		//maximo 5 integrantes por equipo
		if (numeroIntegrantesEquipo > 5) {
			numeroIntegrantesEquipo = 5;

		}

		StartCoroutine(Juego());
	}

	// Se llama al inicio de cada ronda y ejecuta cada fase de cada ronda hasta que el jugador muera.
	private IEnumerator Juego()
	{
		// Iniciar la corutina 'inicioRonda'.
		yield return StartCoroutine(inicioRonda());

		// Ya que termine la corutina 'inicioRonda' iniciar la corutina 'juegoRonda'.
		yield return StartCoroutine(juegoRonda());

		// Ya que termina la corutina 'inicioRonda' inicia 'finRonda'.
		yield return StartCoroutine(finRonda());

		continuar();

	}

	private IEnumerator inicioRonda()
	{
		print("inicio ronda");

		condicionesInicio();

		// Esperar un tiempo para empezar la fase de juego.
		yield return tiempoIni;
	}


	private IEnumerator juegoRonda()
	{
		
		print("juegoRonda");
		msg.text = "";
		//Si no se ha terminado la ronda...
		while (!terminaRonda())
		{
			despliegaBanderaCapturada ();

			for (int i = 0; i < numeroIntegrantesEquipo; i++) {
				
				if (i == 0) {

					if (integrantesAzul [i] == null) {
						integrantesAzul[i] = instanciandoPersonaje(false,equipoAzul,puntoSpawnEquipoAzul[i]);
					}
					if (integrantesRojo [i] == null) {
						integrantesRojo[i] =  instanciandoPersonaje(false,equipoRojo,puntoSpawnEquipoRojo[i]);
					
					}
				
				} 
				else {
					if (integrantesAzul [i] == null) {
						integrantesAzul[i] = instanciandoPersonaje(true,equipoAzul,puntoSpawnEquipoAzul[i]);

					}
					if (integrantesRojo [i] == null) {
						integrantesRojo[i] = instanciandoPersonaje(true,equipoRojo,puntoSpawnEquipoRojo[i]);

					}
				
				}
			
				yield return 100;
			}


			// ... regresar en el siguiente frame.
			yield return null;
		}
	}

	void reiniciarJuego(){

		banderaRojoCapturada = false;
		banderaAzulCapturada = false;
		banderaRojoTirada = false;
		banderaAzulTirada = false;
		puntoParaEquipoRojo = false;
		puntoParaEquipoAzul = false;

		for (int i = 0; i < numeroIntegrantesEquipo; i++) {
			Destroy(integrantesAzul[i]);
			Destroy(integrantesRojo[i]);
		}

		Destroy (BanderaRoja);
		Destroy (BanderaAzul);

		integrantesRojo = new GameObject[numeroIntegrantesEquipo];
		integrantesAzul = new GameObject[numeroIntegrantesEquipo];
	}


	private IEnumerator finRonda()
	{
		print("Fin ronda");
		reiniciarJuego ();
		yield return tiempoFin;
	}



	//Inicializar las variables necesarias para el nivel
	public virtual void condicionesInicio()
	{

		BanderaRoja = instanciarBandera (equipoAzul,posicionInicialBanderaAzul);
		BanderaAzul = instanciarBandera (equipoRojo,posicionInicialBanderaRoja);
		instanciandoEquipos ();

		numRonda++;

		//actualizar numero de ronda en el HUD
		msg.text = "Ronda " + numRonda;
	}

	bool terminaRonda()
	{
		if (puntoParaEquipoAzul == true && puntoParaEquipoRojo == false && banderaAzulCapturada ==false ) {
			rondasGanadasEquipoAzul += 1;
			despliegaGanador (equipoAzul);
			return true;
		} 
		else if (puntoParaEquipoAzul == false && puntoParaEquipoRojo == true && banderaRojoCapturada ==false  ) {
			rondasGanadasEquipoRojo += 1;
			despliegaGanador (equipoRojo);
			return true;
		}
		else
		{
			return false;
		}
	}

	//Funcion para decidir que escena se va a cargar o que se va a hacer al final del la corutina juego
	public virtual void continuar()
	{
		//Si nignuno del equipo ha llegado al numero de rondas para ganar entonces ir a la siguiente ronda
		if (rondasParaGanar > rondasGanadasEquipoAzul && rondasParaGanar > rondasGanadasEquipoRojo) {
			StartCoroutine (Juego ());
		} 
		else {
			//Obtener ganador
			if (rondasGanadasEquipoAzul > rondasGanadasEquipoRojo) {
				msg.text = "El ganador del juego fue el equipo Azul";

			} 
			else {
				msg.text  = "El ganador del juego fue el equipo Rojo";

			}



		}
	}



	public void instanciandoEquipos(){
		for (int i = 0; i < numeroIntegrantesEquipo; i++) {

			if (i == 0) {
				integrantesAzul[i]	= instanciandoPersonaje(false,equipoAzul,puntoSpawnEquipoAzul[i] );
				integrantesRojo[i]  = instanciandoPersonaje(false,equipoRojo,puntoSpawnEquipoRojo[i]);

			} else {

				integrantesAzul[i] = instanciandoPersonaje(true,equipoAzul,puntoSpawnEquipoAzul[i]);
				integrantesRojo[i] = instanciandoPersonaje(true,equipoRojo,puntoSpawnEquipoRojo[i]);
			}


		}

	}

	//metodo para instanciar a los equipos 
	GameObject instanciandoPersonaje( bool IA , string equipo , Transform ubicacion ){
		GameObject gameObjectInstanciado;
		if (IA == true) {
			if (equipo == equipoAzul) {

				//instanciando azules
				GameObject integranteAzul = Instantiate (prefabIA, ubicacion.position, Quaternion.identity) as GameObject; //equipo azul
				integranteAzul.tag = equipoAzul;
				integranteAzul.GetComponent<Renderer> ().material.color = Color.blue;
				//asignando como enemigo el equipo correspondiente
				EnemigoCapturaBandera scriptIntegranteAzul = integranteAzul.GetComponent<EnemigoCapturaBandera>();
				scriptIntegranteAzul.EquipoEnemigo = equipoRojo;
				scriptIntegranteAzul.EquipoAliado = equipoAzul;

				scriptIntegranteAzul.miBasePosicion = baseEquipoAzul.transform;
				scriptIntegranteAzul.baseEnemigaPosicion = baseEquipoRojo.transform;

				gameObjectInstanciado = integranteAzul;

			} else {
				//instanciando rojos
				GameObject integranteRojo = Instantiate (prefabIA, ubicacion.position, Quaternion.identity) as GameObject; //equipo rojo
				integranteRojo.tag = equipoRojo;
				integranteRojo.GetComponent<Renderer> ().material.color = Color.red; 
				//asignando como enemigo el equipo correspondiente
				EnemigoCapturaBandera scriptIntegranteRojo = integranteRojo.GetComponent<EnemigoCapturaBandera>();
				scriptIntegranteRojo.EquipoEnemigo = equipoAzul;
				scriptIntegranteRojo.EquipoAliado = equipoRojo;

				scriptIntegranteRojo.miBasePosicion = baseEquipoRojo.transform;
				scriptIntegranteRojo.baseEnemigaPosicion = baseEquipoAzul.transform;

				gameObjectInstanciado = integranteRojo;
			}
		} 
		else {
			if (equipo == equipoAzul) {
			
				//instanciando personajes
				GameObject personajeAzul = Instantiate(jugador1 , ubicacion.position,Quaternion.identity) as GameObject; //equipo azul
				personajeAzul.tag = equipoAzul;
				personajeAzul.GetComponent<Renderer> ().material.color = Color.blue; 

				gameObjectInstanciado = personajeAzul;
			} else {

				//instanciando rojos
				GameObject personajeRojo = Instantiate(jugador2, ubicacion.position,Quaternion.identity) as GameObject; //equipo rojo
				personajeRojo.tag = equipoRojo;
				personajeRojo.GetComponent<Renderer> ().material.color = Color.red; 

				gameObjectInstanciado = personajeRojo;
			}
			
		}
		return gameObjectInstanciado;

	}

	GameObject instanciarBandera(string equipo , Transform ubicacion){
		GameObject gameObjectInstanciado = null;

		//instanciando las banderas y asignando su tag correspondiente
		//las dos banderas se intancian en el primer spawn de cada equipo
		if (equipo == equipoRojo) {
			GameObject banderaRojoAux = Instantiate (prefabBandera, ubicacion.position, Quaternion.identity) as GameObject;
			banderaRojoAux.tag = tagBanderaEquipoRojo;
			banderaRojoAux.name = tagBanderaEquipoRojo;
			Bandera banderaRojoScript = banderaRojoAux.GetComponent<Bandera> ();
			banderaRojoScript.miEquipo = equipoRojo;
			banderaRojoScript.equipoEnemigo = equipoAzul;
			banderaRojoAux.GetComponent<Renderer> ().material.color = Color.red; 
			gameObjectInstanciado = banderaRojoAux;
		}

		if (equipo == equipoAzul) {
			GameObject banderaAzulAux = Instantiate (prefabBandera, ubicacion.position , Quaternion.identity) as GameObject;
			banderaAzulAux.tag = tagBanderaEquipoAzul;
			banderaAzulAux.name = tagBanderaEquipoAzul;
			banderaAzulAux.GetComponent<Renderer> ().material.color = Color.blue; 
			Bandera banderaAzulScript = banderaAzulAux.GetComponent<Bandera> ();
			banderaAzulScript.miEquipo = equipoAzul;
			banderaAzulScript.equipoEnemigo = equipoRojo;
			gameObjectInstanciado = banderaAzulAux;
		}
		return gameObjectInstanciado;
	}
		

	void despliegaBanderaCapturada(){
		if (banderaAzulCapturada == true && banderaRojoCapturada == false) {
			msg.text = "Bandera Azul capturada";

		} else if (banderaAzulCapturada == false && banderaRojoCapturada == true) {
			msg.text = "Bandera Roja capturada";

		} else if (banderaAzulCapturada == true && banderaRojoCapturada == true) {
			msg.text = "Bandera Roja y Bandera Azul capturada";

		} else if (banderaAzulCapturada == false && banderaRojoCapturada == false) {
			msg.text ="";
		
		}
	}

	void despliegaGanador(string ganador){
		msg.text = "El ganador de la ronda fue: " + ganador;

	}



}
