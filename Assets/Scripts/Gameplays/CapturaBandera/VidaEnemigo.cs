﻿using UnityEngine;
using System.Collections;

public class VidaEnemigo : MonoBehaviour {

    public float vidaInicial = 100;
    public float vidaActual;
    public float regeneration = 5;
    private float timestamp = 0.0f;
    private bool muerto = false;

	[HideInInspector] CapturaBandera capturaBandera;

	[HideInInspector] public string Equipo;

    void Awake()
    {
        vidaActual = vidaInicial;
        InvokeRepeating("Regenerar", 0.0f, 1.0f / regeneration);
    }

	void Start(){

		GameObject gameplay = GameObject.FindGameObjectWithTag ("GamePlay");
		capturaBandera = gameplay.GetComponent<CapturaBandera> ();



	}

    public void RecibeDaño(float cantidad, Vector3 puntoGolpe)
    {

        vidaActual -= cantidad;
        //el tiempo en que recibio daño
        timestamp = Time.time;

        // Poner la posicion del sistema de partículas a la posicion del golpe
        // Sonido, animacion
        //gameObject.SendMessage(accion);
        if (vidaActual <= 0f && !muerto)
        {
            Morir();
            //Actualizar HUD
        }
    }

    void Regenerar()
    {
        //Regenerar vida si lleva 10 segundos sin recibir daño y la vida actual es menor a la vida inicial
        if (vidaActual < vidaInicial && Time.time > (timestamp + 10.0f))
            vidaActual += 1.0f;
    }

    void Morir()
    {
		/*EnemigoCapturaBandera miScript = gameObject.GetComponent<EnemigoCapturaBandera> ();
		Equipo = miScript.EquipoAliado;

		if (Equipo == "EquipoAzul" ) {
			
			capturaBandera.integrantesAzul.Remove (gameObject);

		
		} else {
			capturaBandera.integrantesRojo.Remove (gameObject);
		
		}*/


        muerto = true;
        Destroy(gameObject);
    }

}
