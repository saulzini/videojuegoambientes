﻿using UnityEngine;
using System.Collections;

public class Bandera : MonoBehaviour {

	[HideInInspector] public string equipoAzul  = "EquipoAzul";
	[HideInInspector] public string equipoRojo = "EquipoRojo";

	[HideInInspector] public string miEquipo = "";
	[HideInInspector] public string equipoEnemigo = "";


	[HideInInspector] public bool banderaRecolectable;
	// Use this for initialization
	void Start () {
	
		if (transform.parent != null) {

			banderaRecolectable = false;
		} else {
			banderaRecolectable = true;
		}

	}
	
	// Update is called once per frame
	void Update () {
	

		if (transform.parent != null) {
		
			banderaRecolectable = false;
		} else {
			banderaRecolectable = true;
		}


	}


	void OnTriggerEnter(Collider other) {

		//Si el tag es enemigo entonces

		if (other.gameObject.CompareTag (equipoEnemigo) == true) {
			//Obtener el script de enemigo captura bandera
			EnemigoCapturaBandera scriptEnemigo = other.gameObject.GetComponent<EnemigoCapturaBandera> ();

			VidaPersonajeCapturaBandera personaje = other.gameObject.GetComponent<VidaPersonajeCapturaBandera> (); 

			MeshRenderer meshObjeto = other.gameObject.GetComponent<MeshRenderer> ();

			//Avisar al gameplay que bandera fue la que capturaron 
			//si el equipo enemigo es rojo
			if (miEquipo == equipoAzul) {
			

				//if (CapturaBandera.banderaAzulCapturada == false  || CapturaBandera.banderaAzulTirada== true) {
				if ( (CapturaBandera.banderaAzulCapturada == false || CapturaBandera.banderaAzulCapturada == true) && banderaRecolectable == true ) {
					gameObject.transform.SetParent (other.gameObject.transform);
					meshObjeto.material.color = Color.yellow;
					//la bandera azul fue capturada sino
					CapturaBandera.banderaAzulCapturada = true;
					CapturaBandera.banderaAzulTirada =false;
				}



			} else if (miEquipo == equipoRojo) {
			
				//if (CapturaBandera.banderaRojoCapturada == false || CapturaBandera.banderaRojoTirada == true) {
				if ((CapturaBandera.banderaAzulCapturada == false || CapturaBandera.banderaAzulCapturada == true) && banderaRecolectable==true) {
					gameObject.transform.SetParent (other.gameObject.transform);
					meshObjeto.material.color = Color.green;
					//la roja fue capturada
					CapturaBandera.banderaRojoCapturada = true;
					CapturaBandera.banderaRojoTirada = false;
					
				}
		



			}

			if (scriptEnemigo != null) {
			
				scriptEnemigo.tengoBanderaEnemiga = true;
			} 
			else if (personaje != null) {
			
				personaje.tengoBanderaEnemiga = true;
			}



		} 
		//validando que si agarraron la bandera 
		else if (other.gameObject.CompareTag (miEquipo) == true && banderaRecolectable == true ) {
		
			if (miEquipo == equipoAzul ) {

				gameObject.transform.position = CapturaBandera.posicionInicialBanderaAzul.position; 
				CapturaBandera.banderaAzulCapturada = false;
				CapturaBandera.banderaAzulTirada = false;



			} else {

				gameObject.transform.position = CapturaBandera.posicionInicialBanderaRoja.position;
				CapturaBandera.banderaRojoCapturada = false;
				CapturaBandera.banderaRojoTirada = false;
			}
		
		}
	}
}
