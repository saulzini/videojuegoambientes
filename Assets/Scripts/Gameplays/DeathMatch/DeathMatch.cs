﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class DeathMatch : MonoBehaviour {

	public int numRonda = 0;

	public Text msg;

	public GameObject prefabEquipoAzul;
	public GameObject prefabEquipoRojo;

	[HideInInspector] public List<GameObject> integrantesRojo;
	[HideInInspector] public List<GameObject> integrantesAzul;
	public GameObject baseEquipoRojo;
	public GameObject baseEquipoAzul;


	public int numeroIntegrantesEquipo=5;

	public GameObject jugador1;
	public GameObject jugador2;

	[HideInInspector] public static bool puntoParaEquipoRojo = false;
	[HideInInspector] public static bool puntoParaEquipoAzul = false;
	public Transform[] puntoSpawnEquipoRojo;
	public Transform[] puntoSpawnEquipoAzul;

	private Vida[] vidaEquipoRojo; //Script de vidas del equipo rojo
	private Vida[] vidaEquipoAzul; //Script de vidas del equipo azul


	public float delayInicio = 5f;  //Delay entre el la fase de inicioRonda y la fase juegoRonda
	public float delayFinal = 5f;    //Delay entre la fase de juegoRonda y el finRonda
	private WaitForSeconds tiempoIni;       
	private WaitForSeconds tiempoFin;

	string equipoRojo= "EquipoRojo";
	string equipoAzul= "EquipoAzul";


	public int rondasParaGanar = 3;
	[HideInInspector] public int rondasGanadasEquipoAzul = 0;
	[HideInInspector] public int rondasGanadasEquipoRojo = 0;



	public static string estadoJuego;

	// Inicializando
	void Start () {

		integrantesRojo = new List<GameObject>();
		integrantesAzul = new List<GameObject>();

		tiempoIni = new WaitForSeconds(delayInicio);
		tiempoFin = new WaitForSeconds(delayFinal);

		//maximo 5 integrantes por equipo
		if (numeroIntegrantesEquipo > 5) {
			numeroIntegrantesEquipo = 5;

		}




		StartCoroutine(Juego());
	}

	// Se llama al inicio de cada ronda y ejecuta cada fase de cada ronda hasta que el jugador muera.
	private IEnumerator Juego()
	{
		// Iniciar la corutina 'inicioRonda'.
		yield return StartCoroutine(inicioRonda());

		// Ya que termine la corutina 'inicioRonda' iniciar la corutina 'juegoRonda'.
		yield return StartCoroutine(juegoRonda());

		// Ya que termina la corutina 'inicioRonda' inicia 'finRonda'.
		yield return StartCoroutine(finRonda());

		continuar();

	}

	private IEnumerator inicioRonda()
	{
		print("inicio ronda");
		estadoJuego = "Inicio";
		condicionesInicio();

		// Esperar un tiempo para empezar la fase de juego.
		yield return tiempoIni;
	}



	private IEnumerator juegoRonda()
	{
		int randomNum;
		print("juegoRonda");
		msg.text = "";
		//Si no se ha terminado la ronda...
		while (!terminaRonda())
		{
			estadoJuego = "Juego";


			// ... regresar en el siguiente frame.
			yield return null;
		}
	}

	private IEnumerator finRonda()
	{
		print("Fin ronda");

		puntoParaEquipoAzul = false;
		puntoParaEquipoRojo = false;

		//Quitando enemigos


		for (int i = 0; i < integrantesRojo.Count; i++) {
			Destroy (integrantesRojo [i]);
		}

		for (int i = 0; i < integrantesAzul.Count; i++) {
			Destroy (integrantesAzul [i]);
		}

		integrantesRojo.Clear ();
		integrantesAzul.Clear ();

		yield return tiempoFin;
	}



	//Inicializar las variables necesarias para el nivel
	public virtual void condicionesInicio()
	{

		instanciandoEquipos ();

		numRonda++;

		//actualizar numero de ronda en el HUD
		msg.text = "Ronda " + numRonda;
	}

	bool terminaRonda()
	{
		int numeroIntegrantesRojo = GameObject.FindGameObjectsWithTag (equipoRojo).Length;
		int numeroIntegrantesAzul = GameObject.FindGameObjectsWithTag (equipoAzul).Length;



		if (numeroIntegrantesAzul> 0 && numeroIntegrantesRojo< 1) {
			rondasGanadasEquipoAzul += 1;
			despliegaGanador (equipoAzul);
			return true;
		} else if (numeroIntegrantesAzul< 1 && numeroIntegrantesRojo > 0) {
			rondasGanadasEquipoRojo += 1;
			despliegaGanador (equipoRojo);
			return true;
		} else if (numeroIntegrantesAzul == 0 && numeroIntegrantesRojo == 0) {
			
			despliegaGanador ("ninguno");
			return true;
		}
		else
		{
			return false;
		}
	}

	//Funcion para decidir que escena se va a cargar o que se va a hacer al final del la corutina juego
	public virtual void continuar()
	{
		//Si nignuno del equipo ha llegado al numero de rondas para ganar entonces ir a la siguiente ronda
		if (rondasParaGanar > rondasGanadasEquipoAzul && rondasParaGanar > rondasGanadasEquipoRojo) {
			StartCoroutine (Juego ());
		} 
		else {
			//Obtener ganador
			if (rondasGanadasEquipoAzul > rondasGanadasEquipoRojo) {
				msg.text = "El ganador del juego fue el equipo Azul";

			} 
			else {
				msg.text  = "El ganador del juego fue el equipo Rojo";

			}



		}
	}



	//metodo para instanciar a los equipos 
	void instanciandoEquipos(){

		for (int i = 0; i < numeroIntegrantesEquipo -1 ; i++) {


			//instanciando azules
			GameObject integranteAzul = Instantiate(prefabEquipoAzul , puntoSpawnEquipoAzul[i].position,Quaternion.identity) as GameObject; //equipo azul
			integranteAzul.tag = equipoAzul;
			revivirIntegranteAzul (integranteAzul);
			//instanciando rojos
			GameObject integranteRojo = Instantiate(prefabEquipoRojo , puntoSpawnEquipoRojo[i].position,Quaternion.identity) as GameObject; //equipo rojo
			integranteRojo.tag = equipoRojo;
			revivirIntegranteRojo(integranteRojo);

			//guardando en la lista
			integrantesAzul.Add (integranteAzul);
			integrantesRojo.Add (integranteRojo);
		}

		print ("personaje");
		//instanciando personajes
		GameObject personajeAzul = Instantiate(jugador1 , puntoSpawnEquipoAzul[numeroIntegrantesEquipo-1].position,Quaternion.identity) as GameObject; //equipo azul
		personajeAzul.tag = equipoAzul;
		personajeAzul.GetComponent<Renderer>().material.color = new Color(0, 0, 255); 
		//instanciando rojos
		GameObject personajeRojo = Instantiate(jugador2, puntoSpawnEquipoRojo[numeroIntegrantesEquipo-1].position,Quaternion.identity) as GameObject; //equipo rojo
		personajeRojo.tag = equipoRojo;
		personajeRojo.GetComponent<Renderer>().material.color = new Color(255, 0, 0); 

		//guardando en la lista
		integrantesAzul.Add (personajeAzul);
		integrantesRojo.Add (personajeRojo);

	}


	void despliegaGanador(string ganador){
		msg.text = "El ganador de la ronda fue: " + ganador;

	}

	void revivirIntegranteAzul( GameObject integranteAzul ){
		//cambiando color
		integranteAzul.GetComponent<Renderer>().material.color = new Color(0, 0, 255); 
		//asignando como enemigo el equipo correspondiente
		EnemigoDeathMatch scriptIntegranteAzul = integranteAzul.GetComponent<EnemigoDeathMatch>();
		scriptIntegranteAzul.EquipoEnemigo = equipoRojo;
		scriptIntegranteAzul.EquipoAliado = equipoAzul;




	}

	void revivirIntegranteRojo( GameObject integranteRojo ){
		//cambiando color
		integranteRojo.GetComponent<Renderer>().material.color = new Color(255, 0, 0); 
		//asignando como enemigo el equipo correspondiente
		EnemigoDeathMatch scriptIntegranteRojo = integranteRojo.GetComponent<EnemigoDeathMatch>();
		scriptIntegranteRojo.EquipoEnemigo = equipoAzul;
		scriptIntegranteRojo.EquipoAliado = equipoRojo;




	}

}
