﻿using UnityEngine;
using System.Collections;

public class Bala : MonoBehaviour {
	
     int daño = 50;
    public string dañoA = "Player";
	public string enemigo = "Enemigo";
	public string equipoAzul = "EquipoAzul";
	public string equipoRojo = "EquipoRojo";


	void Start(){

		Destroy (gameObject, 3);
	
	}

	void OnTriggerEnter(Collider other) {
		
		if (other.tag == dañoA || other.tag == enemigo || other.tag == equipoAzul || other.tag == equipoRojo)
        {
			
			print (other.gameObject.name);

			Vida vida = other.GetComponent<Vida>();
			if (vida != null) {
					vida.RecibeDaño (daño, Vector3.one);

			} else {
			
				VidaEnemigo vidaEnemigo = other.GetComponent<VidaEnemigo> ();


				if (vidaEnemigo != null) {
					//ContactPoint contacto = collision.contacts [0];
					vidaEnemigo.RecibeDaño (daño, Vector3.one);

				} else {
					VidaPersonajeCapturaBandera vidaPersonaje = other.GetComponent<VidaPersonajeCapturaBandera> ();
					//ContactPoint contacto = collision.contacts [0];
					if (vidaPersonaje != null) {
						vidaPersonaje.RecibeDaño (daño, Vector3.one);
					} else {
						VidaDestruyeBase vidaDB = other.GetComponent<VidaDestruyeBase> ();
						if (vidaDB != null) {
							vidaDB.RecibeDaño (daño, Vector3.one);
						} else {
							VidaDeathMatch vidaDM = other.GetComponent<VidaDeathMatch> ();

							if (vidaDM != null) {
								vidaDM.RecibeDaño (daño, Vector3.one);
								
							}
						
						}
					}
				}
			
			}

			Destroy (gameObject);
        }
        Destroy(gameObject,3);

	}
}
