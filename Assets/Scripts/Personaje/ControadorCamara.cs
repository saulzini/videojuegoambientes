﻿using UnityEngine;
using UnityEngine.VR;
using System.Collections;

public class ControadorCamara : MonoBehaviour {
	
	// [HideInInspector] variable utilizada sin ser privada pero que no aparece en el inspector


	//Angulo
		float anguloCamara = 60; //angulo actual de la camara
		[HideInInspector] public float anguloCamaraObjetivo = 60; //Punto al que quiere llegar a ver la camara

	//Zoom
		float velocidadZoom = 0.1f; // parametro de velocidad a traves del tiempo
		[HideInInspector] float zoom = 1;  //zoom de la camara al disparar
		[HideInInspector] float velocidadZoomRef; //variable auxiliar para guardar el parametro de velocidad a traves del tiempo

	//Sensibilidad camara
		public float sensibilidadCamara = 5; //Sensibilidad al mover la camara
		//Sensibilidad arma
		public float miraActual = 1; //Sensibilidad del arma de acuerdo a la camara para agregar un poco de delay

	//Movimiento
		[HideInInspector] public float rotacionX ; //posicion en x deseada
		[HideInInspector] public float rotacionY; //posicion en y deaseada
		[HideInInspector] public float rotacionXActual; //posición en x actual
		[HideInInspector] public float rotacionYActual; //posición en y actual
		[HideInInspector] float rotacionVistaX; //velocidad de la rotación actual en x
		[HideInInspector] float rotacionVistaY; //velocidad de la rotacion actual en y
		public float tiempoMovimiento = 0.1f; //tiempo que tarda en llegar al objetivo

	//Posición
		float inputX; //Guardar el eje x de la camara
		float inputY; //Guardar el eje y de la camara
		float posicionX;
		float posicionY;

		public float velocidadLadeoCabeza = 1;
		public float contadorLadeoCabeza;
		public float cantidadLadeoCabezaX = 1;
		public float cantidadLadeoCabezaY = 1;
		public Vector3 emparentarUltimaPosicion;
		public float alturaOjo = 0.9f;

	void Awake(){
		emparentarUltimaPosicion = transform.parent.position;
	}

	/*
	 * El metodo update se ejecuta en funciones de tiempo
	 * irregular por lo que se ocupa FixedUpdate que es exacto
     */

	void Update () {
		
		posicionX = transform.localPosition.x;
		posicionY = transform.localPosition.y;

	    if (transform.parent.GetComponent<ControladorPersonaje> ().tocoTierra)
			contadorLadeoCabeza += Vector3.Distance (emparentarUltimaPosicion, transform.parent.position) * velocidadLadeoCabeza;

		posicionX = Mathf.Sin (contadorLadeoCabeza) * cantidadLadeoCabezaX * miraActual;
		posicionY = (Mathf.Cos (contadorLadeoCabeza * 2) * cantidadLadeoCabezaY * miraActual) + (transform.parent.localScale.y * alturaOjo) - (transform.parent.localScale.y / 2);

		emparentarUltimaPosicion = transform.parent.position;

		//Smooth Damp cambia gradualmente los valores del vector
		if (miraActual == 1)
			zoom = Mathf.SmoothDamp (zoom,1,ref velocidadZoomRef,velocidadZoom); //mayor acercamiento con arma
		else
			zoom = Mathf.SmoothDamp (zoom,0,ref velocidadZoomRef,velocidadZoom); //menor acercamiento con arma

		Camera.main.fieldOfView = Mathf.Lerp (anguloCamaraObjetivo,anguloCamara,zoom); 
		//Se modifica el campo de visión de la camara
		//Lerp regresa el punto medio entre dos puntos

		inputX = Input.GetAxis ("CamaraX"); //Eje horizontal de la camara
		inputY = Input.GetAxis ("CamaraY"); //Eje vertical de la camara

		rotacionY += inputX * sensibilidadCamara * miraActual; //Obtener el eje Y (invertido)
		rotacionX -= inputY * sensibilidadCamara * miraActual; //Obtener el eje X (invertido)

		rotacionX = Mathf.Clamp (rotacionX, -90, 90); //Función para dar un rango de rotación en el eje y

		//SmoothDamp llega a un punto y suavisa el vector para dismunuir el cambio repentido en camara
		rotacionXActual = Mathf.SmoothDamp(rotacionXActual, rotacionX, ref rotacionVistaX, tiempoMovimiento);
		rotacionYActual = Mathf.SmoothDamp(rotacionYActual, rotacionY, ref rotacionVistaY, tiempoMovimiento);

		transform.localRotation = InputTracking.GetLocalRotation (VRNode.CenterEye);

		//transform.rotation = Quaternion.Euler(rotacionXActual, rotacionYActual, 0); //Rotación de la camara
	}
}
