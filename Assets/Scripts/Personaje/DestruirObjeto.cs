﻿using UnityEngine;
using System.Collections;

public class DestruirObjeto: MonoBehaviour {

	public float destruirDespuesDeTiempo = 30; //tiempo para comenzar a destruir
	public float destruirDespuesTiempoAleatorio = 0; //tiempo para destruir aleatoriamente el objeto
	[HideInInspector] float contadorTiempo; 

	void Awake () {
		//Agrega un valor random de tiempo para cada objeto
		destruirDespuesDeTiempo += Random.value * destruirDespuesTiempoAleatorio;
	}

	/* El objeto se destruye cada cierto tiempo */
	void Update(){
		contadorTiempo += Time.deltaTime;
		if (contadorTiempo >= destruirDespuesDeTiempo)
			Destroy (gameObject);
	}
}
