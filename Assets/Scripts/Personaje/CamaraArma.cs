﻿using UnityEngine;
using System.Collections;

public class CamaraArma : MonoBehaviour {

	public GameObject camara;

	// Use this for initialization
	void Awake() {
		camara = GameObject.FindWithTag("MainCamera");
	}
	
	// Update is called once per frame
	void LateUpdate () {
		float camaraAux = gameObject.GetComponent<Camera>().fieldOfView;
		camaraAux = camara.GetComponent<Camera>().fieldOfView;

	}
}
