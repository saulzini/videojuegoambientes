﻿using UnityEngine;
using System.Collections;

public class ControladorBalas : MonoBehaviour {
	float distanciaMaxima = 1000000;
	public GameObject marcaBala; 
	float frenteObjeto = 0.00001f;


	// Update is called once per frame
	void Update () {
		RaycastHit disparo;
		if (Physics.Raycast (transform.position, transform.forward,out disparo, distanciaMaxima)) {
			if (marcaBala && disparo.transform.tag == "ObjetosNivel")
				Instantiate (marcaBala,disparo.point + disparo.normal * frenteObjeto,Quaternion.LookRotation(disparo.normal));
		}
		Destroy (gameObject);
	}
}
