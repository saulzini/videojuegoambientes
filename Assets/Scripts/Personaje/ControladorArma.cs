﻿using UnityEngine;
using System.Collections;

public class ControladorArma : MonoBehaviour {

	//Arma
		public Rigidbody rigidbodyArma; //Referencia al Rigidbody del arma

	//Camara
		[HideInInspector] public GameObject camara;
		float rotacionCamaraX;
		float rotacionCamaraY;
		float rotacionCamara;

	//Rotación
		[HideInInspector] float objetivoRotacionX;
		[HideInInspector] float objetivoRotacionY;
		[HideInInspector] float objetivoRotacionXVelocidadRef;
		[HideInInspector] float objetivoRotacionYVelcidadRef;

	//Velocidad
		float velocidadRotacion = 0.1f;

	//Arma
		//Variables que son utilizadas para ubicar el arma al perosonaje
		public float posicionArmaY = -0.2f;
		public float posicionArmaX = 0.4f;
		public float radioPosicionArma = 1;

		public float velocidadApuntar = 0.05f;
		[HideInInspector] float radioPosicionArmaVista;
		public float radioMira = 0.1f;
		public float miraActual;
		public float zoomAngulo = 30;
		public float retroceso = 0.1f; //Sinergia del disparo en la pistola
		public float tiempoRecuperacion = 0.2f;

	//Disparo 
		public GameObject bala;
		public GameObject generadorBalas;
		public float velocidadDisparo = 15;
		public float tiempoEspera = 3; //tiempo de espera entre balas
		public float anguloDisparoMira = 2; //Precision con arma apuntando
		public float anguloDisparonSinMira = 6; //Precision con arma sin apuntar
		[HideInInspector] public float posicionActualZ;
		[HideInInspector] public float posicionActualZVelocidadRef;
		GameObject sonido;
		public GameObject sonidoBala;
		public GameObject flash;
		GameObject  disparoConFlash;

	//Ladeo del arma al caminar
		public float cantidadLadeoX = 0.01f; //intensidad en la se se ladeara el arma en x
		public float cantidadLadeoY = 0.01f;  //intensidad en la se se ladeara el arma en y
		public float actualLadeoArmaX;
		public float actualLadeoArmaY;

	//Cambio de armas
		public bool tieneArma = false;
		public GameObject cajaArma;
		[HideInInspector] public int contadorLanzamiento = -1;
		[HideInInspector]public Transform personajeTransform;
		[HideInInspector] public ControladorPersonaje controladorPersonaje;

		public GameObject [] objetosModeloArmas;

	//Recarga
		public AudioSource sonidoRecargar;
		bool recargar = false;

	//Municiones
		int tamanoClip  = 25;
		int clipActual = 25;
		int maximaMunicion = 100;
		int municionActual = 100; 

	//Bala
		public Texture balaTextura;
		Rect contadorMunicion = new Rect(25,25,50,25);
		public int  municionInicioX = 100;
		int municionY = 25;
		Vector2 tamanoMunicion = new Vector2(10,25);
		float espacioMunicion = 4;


	public void Awake(){
		contadorLanzamiento = -1;
		personajeTransform = GameObject.FindWithTag ("Personaje").transform;
		controladorPersonaje = GameObject.FindWithTag ("Personaje").GetComponent<ControladorPersonaje> ();
		camara = GameObject.FindWithTag ("MainCamera");
	}

	/*
	 * El metodo update se ejecuta en funciones de tiempo
	 * irregular por lo que se ocupa FixedUpdate que es exacto
     */

	public void Start(){
		//Personaje
		rigidbodyArma =  GetComponent<Rigidbody>(); 
		sonidoRecargar.Play ();
	}

	public void LateUpdate() {

		if (clipActual > tamanoClip)
			clipActual = tamanoClip;

		if (municionActual > maximaMunicion)
			municionActual = maximaMunicion;
		
		if (clipActual < 0)
			clipActual = 0;

		if (municionActual < 0)
			municionActual = 0;


		if (tieneArma) {

			if (!recargar && Input.GetButton ("Recargar") && clipActual < tamanoClip && municionActual > 0) {
				recargar = true;
				//Aqui va la animación de la pistola al recargar
				sonidoRecargar.Play ();
			} 

			if (!recargar && Mathf.Round (Input.GetAxisRaw ("Disparar")) > 0 && clipActual == 0 && municionActual > 0) {
				recargar = true;
				//Aqui va la animación de la pistola al recargar
				sonidoRecargar.Play ();
			}

			if(recargar){
				if (municionActual >= tamanoClip - clipActual) {
					municionActual -= tamanoClip - clipActual;
					clipActual = tamanoClip;
				}
				if (municionActual < tamanoClip - clipActual) {
					clipActual += municionActual;
					municionActual = 0;
				}
				recargar = false;
			}
				
			foreach (GameObject objetoModelo in objetosModeloArmas) {
				objetoModelo.layer = 8;
			}

			rigidbodyArma.useGravity = false;
			cajaArma.GetComponent<Collider> ().enabled = false;
		
			//Camara
			camara.GetComponent<ControadorCamara> ().anguloCamaraObjetivo = zoomAngulo; //Asigna el angulo de visión de la camara
			rotacionCamaraX = camara.GetComponent<ControadorCamara> ().rotacionX; //variable de ControladorCamara X
			rotacionCamaraY = camara.GetComponent<ControadorCamara> ().rotacionY; //variable de ControladorCamara Y

			//Arma (Ladeo al caminar)
			actualLadeoArmaX = Mathf.Sin (camara.GetComponent<ControadorCamara> ().contadorLadeoCabeza) * cantidadLadeoX * radioPosicionArma;
			actualLadeoArmaY = Mathf.Cos (camara.GetComponent<ControadorCamara> ().contadorLadeoCabeza * 2) * cantidadLadeoY * radioPosicionArma;

			//Si el jugador presiona disparar
			if (Mathf.Round (Input.GetAxisRaw ("Disparar")) > 0 && clipActual > 0 && !recargar) {
				if (tiempoEspera <= 0) { //Tiempo para poder volver a disparar

					clipActual -= 1;

					if (bala)
						Instantiate (bala, generadorBalas.transform.position, generadorBalas.transform.rotation); //Instanciar la bala
					if (sonidoBala)
						sonido = GameObject.Instantiate (sonidoBala, generadorBalas.transform.position, generadorBalas.transform.rotation) as GameObject; //Instanciar el sonido
					if (flash)
						disparoConFlash = GameObject.Instantiate (flash, generadorBalas.transform.position, generadorBalas.transform.rotation) as GameObject;
					objetivoRotacionX += (Random.value - 0.5f) * Mathf.Lerp (anguloDisparoMira, anguloDisparonSinMira, radioPosicionArma);
					objetivoRotacionY += (Random.value - 0.5f) * Mathf.Lerp (anguloDisparoMira, anguloDisparonSinMira, radioPosicionArma);
					posicionActualZ -= retroceso; //Animación del arma al disparar
					tiempoEspera = 1;
				}
			}

			tiempoEspera -= Time.deltaTime * velocidadDisparo; //Restar del tiempo en espera

			if (sonido)
				sonido.transform.parent = transform; //El sonido se emparenta con la bala creada
			if (disparoConFlash)
				disparoConFlash.transform.parent = transform;

			posicionActualZ = Mathf.SmoothDamp (posicionActualZ, 0, ref posicionActualZVelocidadRef, tiempoRecuperacion);

			//Si el jugador presiona apuntar
			if (Mathf.Round (Input.GetAxisRaw ("Apuntar")) > 0 && !recargar) {
				camara.GetComponent<ControadorCamara> ().miraActual = radioMira; //Sensibilidad de la pistola (Poco sensible)
				//SmoothDamp llega a un punto y suavisa el vector para dismunuir el cambio repentido en camara
				radioPosicionArma = Mathf.SmoothDamp (radioPosicionArma, 0, ref radioPosicionArmaVista, velocidadApuntar);
			}

			if (Mathf.Round (Input.GetAxisRaw ("Apuntar")) <= 0 || recargar) {
				camara.GetComponent<ControadorCamara> ().miraActual = 1; //Sensibilidad de la pistola (Muy sensible)
				//SmoothDamp llega a un punto y suavisa el vector para dismunuir el cambio repentido en camara
				radioPosicionArma = Mathf.SmoothDamp (radioPosicionArma, 1, ref radioPosicionArmaVista, velocidadApuntar);
			}

			//Vector que define la posición del arma al tenerla el personaje
			Vector3 ubicacionArma = new Vector3 (posicionArmaX * radioPosicionArma + actualLadeoArmaX, posicionArmaY * radioPosicionArma + actualLadeoArmaY, 0);
			Vector3 auxiliar = new Vector3 (0, 0, posicionActualZ);

			//Ubicar el arma
			transform.position = camara.transform.position + Quaternion.Euler (0, objetivoRotacionY, 0) * ubicacionArma + Quaternion.Euler (objetivoRotacionX, objetivoRotacionY, 0) * auxiliar;

			//SmoothDamp llega a un punto y suavisa el vector para dismunuir el cambio repentido en camara
			objetivoRotacionX = Mathf.SmoothDamp (objetivoRotacionX, rotacionCamaraX, ref objetivoRotacionXVelocidadRef, velocidadRotacion);
			objetivoRotacionY = Mathf.SmoothDamp (objetivoRotacionY, rotacionCamaraY, ref objetivoRotacionYVelcidadRef, velocidadRotacion);

			//Ajusta el arma a la rotación de la camara del personaje
			transform.rotation = Quaternion.Euler (objetivoRotacionX, objetivoRotacionY, 0);
		}
		if(!tieneArma) {

			foreach (GameObject objetoModelo in objetosModeloArmas) {
				objetoModelo.layer = 0;
			}

			rigidbodyArma.useGravity = true;
			cajaArma.GetComponent<Collider> ().enabled = true;

			contadorLanzamiento -= 1;
			if (contadorLanzamiento == 0)
				rigidbodyArma.AddRelativeForce (0,controladorPersonaje.tirarArma,controladorPersonaje.tirarDistanciaAdeltante);
		
			if (Vector3.Distance (transform.position, personajeTransform.position) < controladorPersonaje.distanciaRecogerArma
			   && Input.GetButton("Recoger")
			   && controladorPersonaje.esperarCambioArma <= 0) {
					controladorPersonaje.armaActual.GetComponent<ControladorArma> ().tieneArma = false;
					controladorPersonaje.armaActual.GetComponent<ControladorArma> ().contadorLanzamiento = 2;
					controladorPersonaje.armaActual = gameObject;
					tieneArma = true;
					objetivoRotacionY = camara.GetComponent<ControadorCamara> ().rotacionY - 180;
					controladorPersonaje.esperarCambioArma = 2;
			}
		}
	}


	public void  OnGUI(){
		if (tieneArma) {
			int i;
			for (i = 1; i <= clipActual; i++) {
				GUI.DrawTexture(new Rect(municionInicioX + (i - 1 ) * (tamanoMunicion.x + espacioMunicion), municionY, tamanoMunicion.x,tamanoMunicion.y), balaTextura);
			}

			GUI.Label(contadorMunicion,municionActual.ToString());
		}
	}

}
