﻿using UnityEngine;
using System.Collections;
using UnityEngine.VR;

public class ControladorPersonaje : MonoBehaviour {

	//Personaje
		public Rigidbody rigidbodyPersonaje; //Referencia al Rigidbody del personaje

	//Camara
		public GameObject camara;

	//Velocidad
		public float aceleracion = 5;
		public float desaceleracion = 10000000; //distancia y tiempo para desacelerar
		float desaceleracionX; //fricción del personaje en y
		float desaceleracionZ; //fricción del personaje en z
	   float velocidadMaxima = 5; // velocidad maxima del personaje
		
		float velocidadSalto = 500; // velocidad del salto
		float aceleracionEnAire = 0.1f;

	//Rotación
		float rotacionCamara;

	//Posición
		float ejeX; //posicion en x del personaje
		float ejeY; //posicion en y del personaje

	//Movimientos
		float inclinacionMaxima = 60; //inclinación en salto largo
		[HideInInspector] public bool tocoTierra = false; // bandera para saber si esta en el aire o no (Salto)
		Vector2 movimientoHorizontal; //movimientos de personaje

	//Arma
		public GameObject armaActual;
		public float distanciaRecogerArma = 6;
		public float tirarArma = 100;
		public float tirarDistanciaAdeltante = 100;
		public int esperarCambioArma = -1;
		
	public void Start(){
		//Personaje 
		rigidbodyPersonaje =  GetComponent<Rigidbody>(); 
	}

	/*
	 * El metodo update se ejecuta en funciones de tiempo
	 * irregular por lo que se ocupa FixedUpdate que es exacto
     */

	public void LateUpdate(){

		//Ejes
		ejeX = Input.GetAxis ("Horizontal");
		ejeY = Input.GetAxis ("Vertical");

		//Camara
		rotacionCamara = camara.GetComponent<ControadorCamara> ().rotacionYActual; //variable de ControladorCamara

		//Arma
		esperarCambioArma = -1;

		//Movimientos
		movimientoHorizontal = new Vector2 (rigidbodyPersonaje.velocity.x, rigidbodyPersonaje.velocity.z); //movimiento en x
		    
		//Limite de aceleración del personaje
		/*
			Si el personaje supera el limite de velocidad esta
			se mantiene en el limite sin seguir incrementando.
		*/

		if (movimientoHorizontal.magnitude > velocidadMaxima)  
			movimientoHorizontal = movimientoHorizontal.normalized * velocidadMaxima; //Convierte la magnitud del vector en 1
		
	
		//Puntos de vector
		rigidbodyPersonaje.velocity.x.Equals(movimientoHorizontal.x);//Posición actual en x
		rigidbodyPersonaje.velocity.z.Equals(movimientoHorizontal.y); //Posición actual en y

		/*
			Si el personaje salta no puede volver a hacerlo
			hasta que vuelva a tocar el piso.
		*/
		if (tocoTierra) {
			//SmoothDamp llega a un punto y suavisa el vector para dismunuir el cambio repentido en camara
			rigidbodyPersonaje.velocity.x.Equals(Mathf.SmoothDamp (rigidbodyPersonaje.velocity.x, 0, ref desaceleracionX, desaceleracion));
			rigidbodyPersonaje.velocity.z.Equals(Mathf.SmoothDamp (rigidbodyPersonaje.velocity.z, 0, ref desaceleracionZ, desaceleracion));
			//añadir una fuerza al personaje activo
		}
			//rigidbodyPersonaje.rotation = Quaternion.Euler (0, rotacionCamara, 0); //rotación de personaje

		transform.localRotation = InputTracking.GetLocalRotation (VRNode.CenterEye);

		
			if (Input.GetButtonDown ("Salto") && tocoTierra)  //si el jugador presiona saltar y esta en tierra entonces salta
			rigidbodyPersonaje.AddForce (0, velocidadSalto, 0);
		
	}

	void FixedUpdate(){
		if(tocoTierra)
			rigidbodyPersonaje.AddRelativeForce ( ejeX * aceleracion,0,ejeY * aceleracion);
		else
			rigidbodyPersonaje.AddRelativeForce ( ejeX * aceleracion * aceleracionEnAire,0,ejeY * aceleracion * aceleracionEnAire); //añadir una fuerza al personaje activo
	}

	/* Si el jugador esta colisionando con el piso entonces es posible saltar */
	void OnCollisionStay(Collision colision){
		foreach (ContactPoint contacto in colision.contacts) {
			if(Vector3.Angle(contacto.normal, Vector3.up) < inclinacionMaxima)
				tocoTierra = true;
	    }
	}

	/* Si no esta colisionando con el piso entonces es imposible realizar otro salto */
	void OnCollisionExit(){
		tocoTierra = false;
	}
}
