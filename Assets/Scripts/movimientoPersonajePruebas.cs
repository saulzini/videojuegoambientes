﻿using UnityEngine;
using System.Collections;

public class movimientoPersonajePruebas : MonoBehaviour {

	float speed = 5f;

	public GameObject bala;
	private GameObject bala1;
	public Transform  balaPos;

	// Use this for initialization
	void Start () {

	}

	// Update is called once per frame
	void Update ()
	{

		if (Input.GetKey(KeyCode.RightArrow)){
			transform.position += Vector3.right * speed * Time.deltaTime;
		}
		if (Input.GetKey(KeyCode.LeftArrow)){
			transform.position += Vector3.left* speed * Time.deltaTime;
		}
		if (Input.GetKey(KeyCode.UpArrow)){
			transform.position += Vector3.forward * speed * Time.deltaTime;
		}
		if (Input.GetKey(KeyCode.DownArrow)){
			transform.position += Vector3.back* speed * Time.deltaTime;
		}
		if (Input.GetKeyDown(KeyCode.Space))
		{
			Dispara();
		}
	}

	void Dispara()
	{
		// Create the Bullet from the Bullet Prefab
		bala1 = (GameObject)Instantiate(bala, balaPos.position,balaPos.rotation);

		// Add velocity to the bullet
		bala1.GetComponent<Rigidbody>().velocity = bala1.transform.forward * 10;
	}

}
