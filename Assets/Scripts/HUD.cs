﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using Image = UnityEngine.UI.Image;

public class HUD : MonoBehaviour {

    public Image imagenVida;

    //Variables para hacer que la imagen de daño parpadee
    //0.5f toma 2 segundos, 2f toma medio
    public float tiempoFadeImagen = 1f;
    private float alpha;
    private float maxAlpha = .3f;
    private float minAlpha = 0f;
    private float objetivoAlpha = 0.3f;

	// Use this for initialization
	void Start () {
        imagenVida = GameObject.Find("Canvas/Vida").GetComponent<Image>();
    }
	
	// Update is called once per frame
	void Update () {
        //Si la vida es menor a 1/3 aparece la pnatalla roja y parpadea
	    if (GetComponent<Vida>().vidaActual < (GetComponent<Vida>().vidaInicial / 3.0f))
        {
            imagenVida.enabled = true;
            alpha = Mathf.MoveTowards(imagenVida.color.a, objetivoAlpha, Time.deltaTime * tiempoFadeImagen * 0.3f);
            if (alpha >= maxAlpha)
            {
                alpha = maxAlpha;
                objetivoAlpha = minAlpha;
            }
            else if (alpha <= minAlpha)
            {
                alpha = minAlpha;
                objetivoAlpha = maxAlpha;
            }
            imagenVida.color = new Color(imagenVida.color.r, imagenVida.color.g, imagenVida.color.b, alpha);
        }
        else
        {
            imagenVida.enabled = false;
        }
	}

    
}
