﻿using UnityEngine;
using System.Collections;

public class VidaEnemigosBM : MonoBehaviour {

    public float vidaInicial = 10;
    public float vidaActual;
    public float cantidadVidaRegenerar = 5;
    public int valorPuntuacion = 10; //Puntos que obtiene el jugador por matar a un enemigo
    private float timestamp = 0.0f;
    private bool muerto = false;

    void Awake()
    {
        vidaActual = vidaInicial;
        InvokeRepeating("Regenerar", 0.0f, 1.0f / cantidadVidaRegenerar);
    }

    public void RecibeDaño(float cantidad, Vector3 puntoGolpe)
    {

        vidaActual -= cantidad;
        //el tiempo en que recibio daño
        timestamp = Time.time;

        // Poner la posicion del sistema de partículas a la posicion del golpe
        // Sonido, animacion
        //gameObject.SendMessage(accion);
        if (vidaActual <= 0f && !muerto)
        {
            Morir();
            //Actualizar HUD
        }
    }

    void Regenerar()
    {
        //Regenerar vida si lleva 10 segundos sin recibir daño y la vida actual es menor a la vida inicial
        if (vidaActual < vidaInicial && Time.time > (timestamp + 10.0f))
            vidaActual += 1.0f;
    }

    void Morir()
    {
        muerto = true;
        print("Muerto");
        PuntuacionIndividual.puntos += valorPuntuacion;
        Destroy(gameObject, 3f);
    }

}
