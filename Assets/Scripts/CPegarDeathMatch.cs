﻿using UnityEngine;
using System.Collections;

public class CPegarDeathMatch: MonoBehaviour {

	//creando instancia de enemigo
	EnemigoDeathMatch enemigoScript;

	void Awake (){


		if (enemigoScript == null) {
			enemigoScript = GetComponentInParent<EnemigoDeathMatch>();
		}
	
	}


	void Start(){




	}

	//Mientras el player esta dentro del collider entonces dispara
	void OnTriggerStay(Collider other)
	{

		//comparando si es el jugador
		if ( other.gameObject.tag == enemigoScript.EquipoEnemigo == true)
		{
			enemigoScript.pega = true;
		}
	}

	//Si se salio del collider entonces es falso
	void OnTriggerExit(Collider other)
	{
		//comparando si es el jugador
		if ( other.gameObject.tag == enemigoScript.EquipoEnemigo == true )
		{
			enemigoScript.pega = false;
		}
	}
}
