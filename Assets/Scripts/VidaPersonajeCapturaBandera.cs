﻿using UnityEngine;
using System.Collections;

public class VidaPersonajeCapturaBandera : MonoBehaviour {

    public float vidaInicial = 100;
    public float vidaActual;
    public float cantidadVidaRegenerar = 5;
    private float timestamp = 0.0f;
    private bool muerto = false;

	//variables estaticas para guardar los equipos y para comparar
	[HideInInspector] public string equipoRojo = "EquipoRojo";
	[HideInInspector] public string equipoAzul = "EquipoAzul";
	[HideInInspector] public string miEquipo = "";

	[HideInInspector] public bool tengoBanderaEnemiga = false;
	[HideInInspector] public string BanderaRojo = "BanderaRojo";
	[HideInInspector] public string BanderaAzul = "BanderaAzul";

    void Awake()
    {
        vidaActual = vidaInicial;


        //InvokeRepeating("Regenerar", 0.0f, 1.0f / cantidadVidaRegenerar);

	}

	public void Start(){
		miEquipo = gameObject.tag;

	}

    public void RecibeDaño(float cantidad, Vector3 puntoGolpe)
    {

        vidaActual -= cantidad;
        //el tiempo en que recibio daño
        timestamp = Time.time;

        // Poner la posicion del sistema de partículas a la posicion del golpe
        // Sonido, animacion
        //gameObject.SendMessage(accion);
        if (vidaActual <= 0f && !muerto)
        {
            Morir();
            //Actualizar HUD
        }
    }

    void Regenerar()
    {
        //Regenerar vida si lleva 10 segundos sin recibir daño y la vida actual es menor a la vida inicial
        if (vidaActual < vidaInicial && Time.time > (timestamp + 10.0f))
            vidaActual += 1.0f;
    }

    void Morir()
    {


        muerto = true;
		print("Muerto:"+gameObject.name);
        Destroy(gameObject);
    }

	//Si se destruye el gameobject y si tiene la bandera entonces crear la bandera en ese punto
	void OnDestroy(){

		if (tengoBanderaEnemiga == true ) {

			//validando de que equipo soy
			//para quitar al hijo
			if (miEquipo == equipoAzul) {

				GameObject bandera = gameObject.transform.FindChild(BanderaRojo).gameObject;
				bandera.transform.SetParent (null);
				Bandera banderaScript = bandera.GetComponent<Bandera> ();
				banderaScript.enabled = true;

				CapturaBandera.banderaRojoTirada = true;

			} else if (miEquipo == equipoRojo) {
				GameObject bandera = gameObject.transform.FindChild(BanderaAzul).gameObject;
				bandera.transform.SetParent (null);
				CapturaBandera.banderaAzulTirada = true;
				Bandera banderaScript = bandera.GetComponent<Bandera> ();
				banderaScript.enabled = true;

			}

			tengoBanderaEnemiga = false;

		}

	}

	

}
